﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public class CameraCore : MonoBehaviour
    {
        Camera _camera;

        byte gridWidth, gridHeight;

        private void Start()
        {
            _camera = gameObject.AddComponent<Camera>();
            _camera.backgroundColor = Color.black;
            _camera.clearFlags = CameraClearFlags.Color;
            _camera.orthographic = true;
        }

#if UNITY_EDITOR
        private void Update()
        {
            if(gridWidth > 0 && gridHeight > 0)
            {
                OrtographicSize(gridWidth, gridHeight);
            }
        }
#endif

        public void SetPositionFromGrid(byte width, byte height, byte depth = 0)
        {
            this.gridWidth = width;
            this.gridHeight = height;

            Vector3 pos = Vector3.zero;
            pos.x = width * 0.5f;
            pos.y = height * 0.5f;
            pos.z = -(width > height ? width : height) * 2;

            transform.localPosition = pos;

            OrtographicSize(width, height);
        }

        void OrtographicSize(byte width, byte height)
        {
            // to keep grid in the exact camera field
            GetComponent<Camera>().orthographicSize = Screen.width > Screen.height ? width : height * 1 / ((float)Screen.width / (float)Screen.height);
            GetComponent<Camera>().orthographicSize *= 0.5f;

            // with a margin (to avoid UI overlaping on game area)
            GetComponent<Camera>().orthographicSize += 2;

        }
    }
}
