﻿using UnityEngine;

namespace Atari
{
    public static class TransformExtensions
    {
        public static void Reset(this Transform tr)
        {
            tr.localPosition = Vector3.zero;
            tr.localEulerAngles = Vector3.zero;
            tr.localScale = Vector3.one;
        }
    }
}