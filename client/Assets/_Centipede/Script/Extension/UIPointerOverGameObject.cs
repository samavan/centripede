﻿using UnityEngine;
using UnityEngine.EventSystems;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Atari
{
	public class UIPointerOverGameObject : Singleton<UIPointerOverGameObject>
	{
		[HideInInspector]
		public bool state;
		public bool lateState;

		void Update()
		{
			 state = IsPointerOverGameObject();
		}

		public bool IsPointerOverGameObject()
		{
			if(TouchOnUI()){return true;}
			//  Debug.Log(EventSystem.current.IsPointerOverGameObject() + " || " + !MouseInGameView());
			return EventSystem.current.IsPointerOverGameObject(); // || !MouseInGameView();
		}

		public bool TouchOnUI()
		{
			foreach (Touch touch in Input.touches)
			{
				int id = touch.fingerId;
				if (EventSystem.current.IsPointerOverGameObject(id))
				{
					return true;
				}
			}

			return false;
		}

		public bool MouseInGameView()
		{
        #if UNITY_EDITOR
			if (Input.mousePosition.x == 0 || Input.mousePosition.y == 0 || Input.mousePosition.x >= Handles.GetMainGameViewSize().x - 1 || Input.mousePosition.y >= Handles.GetMainGameViewSize().y - 1)
			{
				return false;
			}
		#else
			if (Input.mousePosition.x == 0 || Input.mousePosition.y == 0 || Input.mousePosition.x >= Screen.width - 1 || Input.mousePosition.y >= Screen.height - 1)
			{
				return false;
			}
		#endif
			return true;
		}
	}
}