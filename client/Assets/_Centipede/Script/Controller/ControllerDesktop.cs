﻿using UnityEngine;

namespace Atari.Centripede
{
    public class ControllerDesktop : MonoBehaviour
    {
        ComponentAssetPlayer player;

        bool playerMove;
        bool playerShoot;

        private void Start()
        {
            enabled = false;
        }

        private void Update()
        {
            if(playerMove)
            {
                PlayerControllerMove();
            }

            if(playerShoot && !sAppUI.Instance.uiPointerOverGameObject.state)
            {
                PlayerControllerShoot();
            }
        }

        public void State(bool state, ComponentAssetPlayer player=null)
        {
            this.player = player;
            if (state)
            {
                playerMove = playerShoot = true;
            }
            enabled = state;
        }


        // ==================================== //
        // ======== Player Move =============== //
        // ==================================== //

        void PlayerControllerMove()
        {
            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                PlayerMove(ENTITY_DIRECTION.LEFT);
            }
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                PlayerMove(ENTITY_DIRECTION.RIGHT);
            }
            else if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                PlayerMove(ENTITY_DIRECTION.UP);
            }
            else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            {
                PlayerMove(ENTITY_DIRECTION.DOWN);
            }
        }

        void PlayerMove(ENTITY_DIRECTION dir)
        {
            // to keep active if the player cannot move (obstacle or grid border)
            playerMove = !player.MoveTo(dir, OnPlayerMoved);
        }

        void OnPlayerMoved()
        {
            playerMove = true;
        }

        // ==================================== //
        // ======== Player Shoot ============== //
        // ==================================== //

        void PlayerControllerShoot()
        {
            if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
            {
                PlayerShoot();
            }
        }

        void PlayerShoot()
        {
            playerShoot = false;
            player.Shoot(OnShootReady);
        }

        void OnShootReady()
        {
            playerShoot = true;
        }
    }
}
