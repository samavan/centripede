﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Atari.Centripede
{
    public partial class sAppUI : Singleton<sAppUI>
    {
		[HideInInspector]
		public UI_Debug uiDebug;

		[HideInInspector]
		public UI_IG_Start uiIGStart;
		[HideInInspector]
		public UI_IG_Main uiIGMain;

		EventSystem eventSystem;
		StandaloneInputModule inputModule;
		[HideInInspector]
		public UIPointerOverGameObject uiPointerOverGameObject;

		private void Start()
        {
			eventSystem = new GameObject("EventSystem").AddComponent<EventSystem>();
			eventSystem.transform.parent = transform;
			eventSystem.transform.Reset();
			
			uiPointerOverGameObject = gameObject.AddComponent<UIPointerOverGameObject>();
			
			inputModule = eventSystem.gameObject.AddComponent<StandaloneInputModule>();

			uiDebug = LoadUI("", "UI_Debug").GetComponent<UI_Debug>();
			uiIGStart = LoadUI("", "UI_IG_Start").GetComponent<UI_IG_Start>();
			uiIGMain = LoadUI("", "UI_IG_Main").GetComponent<UI_IG_Main>();

			uiDebug.State(true, sApp.Instance.defaultGameData, sApp.Instance.OnDebugMenuValid);
		}

		public void UIDebug(bool state)
        {
            uiDebug.State(state);
        }

		public GameObject LoadUI(string subDir, string name)
		{
			// Debug.Log("s_AssetEditorUI.LoadUI(" + subDir + ", " + name + ");");

			string resourceName = ManagerApp.Resources.ui + name;

			GameObject goResource = Resources.Load(resourceName) as GameObject;
			if (goResource)
			{
				GameObject goUI = Instantiate(goResource) as GameObject;
				goUI.name = goUI.name.Replace("(Clone)", "");
				// DestroyImmediate(goResource); // It seems the Singleton class conflict with the class Loaded from the resources and the copy...
				goUI.transform.SetParent(transform);
				return goUI;
			}
			else
			{
				Debug.Log("<color=red>Unable to load : </color>" + resourceName + ";");
			}
			return null;
		}

		public void NewGame(byte playerLife)
		{
			uiIGMain.Reset(playerLife);
		}

		public void GameReady()
		{
			uiIGStart.State(true, sApp.Instance.OnPressStart);
		}

		public void GameStart()
		{
			uiIGMain.State(true);
		}

		public void ScoreAdd(int score, int value)
		{
			uiIGMain.ScoreAdd(score, value);
		}

		public void SetLife(byte value)
		{
			uiIGMain.SetLife(value);
		}

		public void GameOver()
		{
			uiIGMain.State(false);
			uiIGStart.State(true, sApp.Instance.OnPressStart);
		}
	}
}