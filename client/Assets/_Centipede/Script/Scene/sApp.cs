﻿using UnityEngine;

namespace Atari.Centripede
{
    public partial class sApp : Singleton<sApp>
    {
        [HideInInspector]
        public GameData defaultGameData;
        public GameData gameData;   // realtime data.
        PlayerData playerData;

        CameraCore cameraCore;
        ControllerDesktop controller;
        
        // factory
        EnemyFactory enemyFactory;
        GridFactory gridFactory;
        PathFindingFactory pathFindingFactory;
        PlayerFactory playerFactory;

        sAppUI uiCore;

        private void Start()
        {
            defaultGameData = new GameData();

            uiCore = NewRoot("UI").AddComponent<sAppUI>();
            cameraCore = NewRoot("Camera").AddComponent<CameraCore>();
            controller = NewRoot("Controller").AddComponent<ControllerDesktop>();

            enemyFactory = NewRoot("Enemy").AddComponent<EnemyFactory>();
            enemyFactory.onCentripedeKilled = OnCentripedeKilled;
            enemyFactory.onSpiderKilled = OnSpiderKilled;

            gridFactory = NewRoot("Grid").AddComponent<GridFactory>();
            pathFindingFactory = NewRoot("PathFinding").AddComponent<PathFindingFactory>();
            playerFactory = NewRoot("Player").AddComponent<PlayerFactory>();
        }

        GameObject NewRoot(string name)
        {
            GameObject go = new GameObject("_" + name + "_");
            go.transform.parent = transform;
            go.transform.Reset();
            return go;
        }


        public Vector2 PlayerPosition()
        {
            return playerData.component.transform.position;
        }

        public CellData PlayerUpperCell()
        {
            return playerData.component.GetUpperCell();
        }

        public void SaveGrid()
        {
            gridFactory.Save();
        }

    }
}