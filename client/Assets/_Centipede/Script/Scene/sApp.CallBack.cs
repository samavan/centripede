﻿using System.Collections;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class sApp : Singleton<sApp>
    {
        float replayDelay = 2f;


        public void OnDebugMenuValid(GameData gameData)
        {
            this.defaultGameData = gameData;
            OnNewGame();
        }

        void OnNewGame()
        {
            gameData.score = 0;
            gameData.playerLife = defaultGameData.playerLife;

            switch(defaultGameData.levelType)
            {
                case LEVEL_TYPE.LOAD:
                    gridFactory.NewGridFromData(gridFactory.Load(defaultGameData.levelToLoad),
                        defaultGameData.mushroomLife);
                    break;
                case LEVEL_TYPE.RANDOM:
                    gridFactory.NewRandomGrid(
                        new GridData
                        {
                            width = defaultGameData.gridWidth,
                            height = defaultGameData.gridHeight,
                            depth = 0
                        },
                        defaultGameData.mushroomCount,
                        defaultGameData.mushroomLife);
                    break;
            }

            playerData = playerFactory.SetPlayer(
                defaultGameData.playerLife, defaultGameData.playerSpeed,
                defaultGameData.playerShootSpeed,
                gridFactory.playerStartCell,
                // 15% is the constraint but not mentionned if floor or ceil then...
                Mathf.FloorToInt((float)defaultGameData.gridHeight * 0.15f));

            cameraCore.SetPositionFromGrid(defaultGameData.gridWidth, defaultGameData.gridHeight);            

            uiCore.NewGame(defaultGameData.playerLife);

            //StartCoroutine(WaitToStart());
            uiCore.GameReady();
        }

        public void OnPressStart()
        {
            gridFactory.ClearData();

            playerData = playerFactory.SetPlayer(
                defaultGameData.playerLife, defaultGameData.playerSpeed,
                defaultGameData.playerShootSpeed,
                gridFactory.playerStartCell,
                Mathf.FloorToInt((float)defaultGameData.gridHeight * 0.15f));

            playerData.component.State(true, OnPlayerDeath);

            enemyFactory.SetCentipede(
                defaultGameData.centripedeLife,
                defaultGameData.centripedeSpeed,
                gridFactory.centripedeStartCell);

            enemyFactory.NewSpider(
                defaultGameData.spiderSpeed,
                defaultGameData.spiderSpawnDelay,
                gridFactory.spiderStartCell,
                defaultGameData.spiderDebug);

            enemyFactory.MobsState(true);

            controller.State(true, playerData.component);
            uiCore.GameStart();
        }

        public void OnScoreAdd(int value)
        {
            gameData.score += value;
            uiCore.ScoreAdd(gameData.score, value);
        }

        void OnCentripedeKilled()
        {
            GameEnd();
            currentCellIdx = 0;
            showBlockDelay = replayDelay / 5;
            cellsToShow = gridFactory.AddRandomBlock(5);

            foreach(CellData cell in cellsToShow)
            {
                cell.componentCell.State(false);
            }
            StartCoroutine(ShowNewBlock());
        }

        void OnSpiderKilled()
        {
            OnScoreAdd(200);
        }

        void OnPlayerDeath()
        {
            GameEnd();
            gameData.playerLife--;

            if(gameData.playerLife <= 0)
            {
                OnGameOver();
                return;
            }

            uiCore.SetLife(gameData.playerLife);
            StartCoroutine(WaitToStart());
            //uiCore.GameReady();
        }

        void GameEnd()
        {
            controller.State(false);
            enemyFactory.MobsState(false);
            pathFindingFactory.Reset();
            playerData.component.enabled = false;
        }

        void OnGameOver()
        {
            uiCore.GameOver();
            controller.State(false);
        }

        IEnumerator WaitToStart()
        {
            yield return new WaitForSeconds(replayDelay);
            OnPressStart();
        }


        // progressivly show new cell

        float showBlockDelay;
        CellData[] cellsToShow;
        byte currentCellIdx;

        IEnumerator ShowNewBlock()
        {
            cellsToShow[currentCellIdx].componentCell.State(true);
            yield return new WaitForSeconds(showBlockDelay);
            currentCellIdx++;
            if (currentCellIdx < cellsToShow.Length)
            {
                StartCoroutine(ShowNewBlock());
            }
            else
            {
                OnPressStart();
            }
        }
    }
}