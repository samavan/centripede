﻿namespace Atari.Centripede
{
    public enum LEVEL_TYPE
    {
        /// <summary>
        /// DO NOT EDIT THE ENUM ORDER 
        /// User for UI and could lead to errors.
        /// </summary>
        RANDOM,
        LOAD
    }
}
