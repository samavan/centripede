﻿namespace Atari.Centripede
{
    public class PlayerData
    {
        public ComponentAssetPlayer component;

        public byte life;
        public byte speed;     // speed = 1 >>> 1 cell/s (1m/s)
        public int yMax;        // (15% of the grid)
        
        // projectile
        public byte shootSpeed;
    }
}
