﻿using UnityEngine;

namespace Atari.Centripede
{
    public class PlayerFactory : MonoBehaviour
    {
        string resAsset = "PLY_00";
        PlayerData player;

        public PlayerData SetPlayer(byte life, byte speed, byte shootSpeed, CellData cellStart, int yMax)
        {
            if (player == null)
            {
                NewPlayer();
            }
            
            player.life = life;
            player.speed = speed;
            player.yMax = yMax;
            player.shootSpeed = shootSpeed;
            player.component.Init(cellStart);
            return player;
        }

        void NewPlayer()
        {
            player = new PlayerData();
            player.component = (Instantiate(Resources.Load(ManagerApp.Resources.player + resAsset)) as GameObject).GetComponent<ComponentAssetPlayer>();
            player.component.SetPlayerData(player);
            player.component.gameObject.name = "player-" + resAsset;
            player.component.transform.parent = transform;
            player.component.transform.Reset();
        }
    }
}
