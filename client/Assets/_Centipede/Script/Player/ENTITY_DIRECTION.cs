﻿namespace Atari.Centripede
{
    public enum ENTITY_DIRECTION
    {
        NONE,
        LEFT,
        RIGHT,
        UP,
        DOWN
    }
}
