﻿using UnityEngine;
using UnityEngine.UI;

namespace Atari
{
    public class UI_InputField_ClampUShort : MonoBehaviour
    {
        public ushort min = 0;
        public ushort max = 65535;

        private void Start()
        {
            InputField input = gameObject.GetComponent<InputField>();
            input.onEndEdit.AddListener(delegate { OnValueChanged(input); });
        }

        public void OnValueChanged(InputField input)
        {
            input.text = Mathf.Clamp(int.Parse(input.text), min, max).ToString();
        }
    }
}
