﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Atari.Centripede
{
    public class UI_IG_Main : UI_Base
    {
        public GameObject goScore;
        Text [] txtScore;
        public GameObject goLifeTemplate;
        public GameObject goLifeLose;

        List<GameObject> goLifes = new List<GameObject>();

        byte life;

        private void Start()
        {
            txtScore = goScore.GetComponentsInChildren<Text>(true);
            goLifeLose.SetActive(false);
            goLifeTemplate.SetActive(false);
            State(false);
        }

        public void Reset(byte life)
        {
            ScoreAdd(0,0);
            LifeInit(life);
        }

        public void ScoreAdd(int score, int value)
        {
            foreach(Text txt in txtScore)
            {
                txt.text = score.ToString();
            }

            if (value > 0)
            {
                goScore.SetActive(false);
                goScore.SetActive(true);
            }
        }

        public void LifeInit(byte life)
        {
            this.life = life;
            // to add more life icon 
            while(life > goLifes.Count)
            {
                GameObject go = Instantiate(goLifeTemplate) as GameObject;
                go.name = (goLifes.Count + 1) + " - Life";
                go.transform.SetParent(goLifeTemplate.transform.parent);
                go.transform.Reset();
                goLifes.Add(go);
            }

            // that will never be a long loop then it doesn't really matter.
            for(int i=0; i < goLifes.Count; i++)
            {
                goLifes[i].SetActive(i < life);
            }
        }

        public void SetLife(byte life)
        {
            if (life < this.life)
            {
                goLifeLose.SetActive(false);
                goLifeLose.SetActive(true);
            }
            this.life = life;
            goLifes[0].gameObject.SetActive(false);
        }

        public void State(bool state)
        {
            goRoot.SetActive(state);
        }


        public void BtnSaveLevel()
        {
            sApp.Instance.SaveGrid();
        }
    }
}