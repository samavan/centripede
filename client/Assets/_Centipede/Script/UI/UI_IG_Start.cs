﻿using System;
using UnityEngine;


namespace Atari.Centripede
{
    public class UI_IG_Start : UI_Base
    {
        Action onStart;

        private void Start()
        {
            State(false);
        }

        private void Update()
        {
            if(UIPointerOverGameObject.Instance.state)
            {
                return;

            }
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                onStart();
                State(false);
            }
        }

        public void BtnShowDebug()
        {
            sAppUI.Instance.UIDebug(true);
            State(false);
        }

        public void State(bool state, Action onStart=null)
        {
            this.onStart = onStart;
            goRoot.SetActive(state);
            enabled = state;
        }
    }
}