﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Atari.Centripede
{
    public class UI_Debug : UI_Base
    {
        public GameObject goLevelLoad;
        public GameObject goLevelRandom;

        public Dropdown dropDownLevels;

        public InputField inputGridWidth;
        public InputField inputGridHeight;
        public InputField inputMushroomCount;
        public InputField inputMushroomLife;
        public InputField inputPlayerLife;
        public InputField inputPlayerSpeed;
        public InputField inputPlayerShootSpeed;
        public InputField inputCentripedeLife;
        public InputField inputCentripedeSpeed;
        public InputField inputSpiderSpeed;
        public InputField inputSpiderSpawnDelay;
        public Toggle toggleSpideDebug;

        GameData data;
        Action<GameData> onValid;

        LEVEL_TYPE levelType = LEVEL_TYPE.RANDOM;

        private void Start()
        {
            OnLevelTypeChange((int)levelType);
            LevelListInit();
        }

        void LevelListInit()
        {
            dropDownLevels.ClearOptions();
            TextAsset[] taLs = Resources.LoadAll<TextAsset>(ManagerApp.Resources.level);
            List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
            foreach (TextAsset ta in taLs)
            {
                options.Add(new Dropdown.OptionData
                {
                    text = ta.name
                });
            }
            dropDownLevels.AddOptions(options);
        }

        public void BtnBalid()
        {
            data.levelType = levelType;
            data.levelToLoad = dropDownLevels.captionText.text;
            data.gridWidth = byte.Parse(inputGridWidth.text);
            data.gridHeight = byte.Parse(inputGridHeight.text);
            data.mushroomCount = ushort.Parse(inputMushroomCount.text);
            data.mushroomLife = byte.Parse(inputMushroomLife.text);
            data.playerLife = byte.Parse(inputPlayerLife.text);
            data.playerSpeed = byte.Parse(inputPlayerSpeed.text);
            data.playerShootSpeed = byte.Parse(inputPlayerShootSpeed.text);
            data.centripedeLife = byte.Parse(inputCentripedeLife.text);
            data.centripedeSpeed = byte.Parse(inputCentripedeSpeed.text);
            data.spiderSpeed = byte.Parse(inputSpiderSpeed.text);
            data.spiderSpawnDelay = byte.Parse(inputSpiderSpawnDelay.text);
            data.spiderDebug = toggleSpideDebug.isOn;

            onValid(data);
            State(false);
        }

        public void OnLevelTypeChange(int value)
        {
            levelType = (LEVEL_TYPE)value;
            goLevelLoad.SetActive(levelType == LEVEL_TYPE.LOAD);
            goLevelRandom.SetActive(levelType == LEVEL_TYPE.RANDOM);
        }

        public void State(bool state, GameData data, Action<GameData> onValid)
        {
            this.data = data;
            this.onValid = onValid;
            State(state);
        }
        public void State(bool state)
        {
            goRoot.gameObject.SetActive(state);
        }
    }
}