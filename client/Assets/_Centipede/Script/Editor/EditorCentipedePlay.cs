﻿using UnityEditor;
using UnityEditor.SceneManagement;

namespace Atari.Centripede
{
    public partial class EditorCentipedePlay : Editor
    {
        [MenuItem("Centipede/Play")]
        public static void PlayScene()
        {
            EditorSceneManager.OpenScene("Assets/_Centipede/Scene/sApp.unity");
            EditorApplication.isPlaying = true;
        }
    }
}