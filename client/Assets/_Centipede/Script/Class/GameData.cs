﻿namespace Atari.Centripede
{
    public struct GameData
    {
        public int score;

        public LEVEL_TYPE levelType;
        public string levelToLoad;

        public byte gridWidth;
        public byte gridHeight;

        public ushort mushroomCount;
        public byte mushroomLife;

        public byte playerLife;
        public byte playerSpeed;
        public byte playerShootSpeed;

        public byte centripedeLife;
        public byte centripedeSpeed;

        public byte spiderSpeed;
        public byte spiderSpawnDelay;
        public bool spiderDebug;
    }
}
