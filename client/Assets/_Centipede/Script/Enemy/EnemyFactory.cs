﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public class EnemyFactory : Singleton<EnemyFactory>
    {
        public void MobsState(bool state)
        {
            foreach (ComponentAssetEnemyCentipede part in centipedeParts)
            {
                part.enabled = state;
            }

            if (state)
            {
                spider.Spawn();
            }
            else
            {
                spider.enabled = false;
            }
        }

        // ======== CENTRIPEDE ======== //

        public Action onCentripedeKilled;
        string resCentripede = "ENM_Centripede_00";
        List<ComponentAssetEnemyCentipede> centipedeParts = new List<ComponentAssetEnemyCentipede>();

        public void SetCentipede(byte partsCount, byte speed, CellData cell)
        {
            for (int i = 0; i < partsCount; i++)
            {
                if (centipedeParts.Count <= i)
                {
                    centipedeParts.Add(NewCentipedePart());
                }
                centipedeParts[i].SetBodyPart(i < partsCount - 1 ? CENTIPEDE_BODYPART.BODY : CENTIPEDE_BODYPART.HEAD);
                centipedeParts[i].Init(cell, speed, OnCentripedePartDeath);
                centipedeParts[i].name = "Centripede-" + i;
                cell = cell.cellRight;               
            }

            // to make the connection between each parts

            for (int i = 0; i < partsCount; i++)
            {
                if (i > 0)
                {
                    centipedeParts[i].nextPart = centipedeParts[i - 1];
                }
                if (i < partsCount - 1)
                {
                    centipedeParts[i].previousPart = centipedeParts[i + 1];
                }
            }
        }

        ComponentAssetEnemyCentipede NewCentipedePart()
        {
            GameObject go = Instantiate(Resources.Load(ManagerApp.Resources.enemy + resCentripede)) as GameObject;
            go.name = resCentripede;
            go.transform.parent = transform;
            go.transform.Reset();            
            return go.GetComponent<ComponentAssetEnemyCentipede>();            
        }

        void OnCentripedePartDeath()
        {
            foreach (ComponentAssetEnemyCentipede part in centipedeParts)
            {
                if (part.enabled)
                {
                    return;
                }
            }
            onCentripedeKilled();
        }

        // ======== SPIDER ======== //

        public Action onSpiderKilled;
        string resSpider = "ENM_Spider_00";
        ComponentAssetEnemySpider spider;

        public ComponentAssetEnemySpider NewSpider(byte speed, byte spawnDelay, CellData cell, bool debug=false)
        {
            if (!spider)
            {
                // Debug.Log("New Spider");
                GameObject go = Instantiate(Resources.Load(ManagerApp.Resources.enemy + resSpider)) as GameObject;
                go.name = resSpider;
                go.transform.parent = transform;
                go.transform.Reset();
                spider = go.GetComponent<ComponentAssetEnemySpider>();
            }
            spider.debugTrajectory = debug;
            spider.Init(cell, speed, spawnDelay, onSpiderKilled);
            return spider;
        }
    }
}
