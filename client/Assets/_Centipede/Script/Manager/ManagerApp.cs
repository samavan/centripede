﻿namespace Atari.Centripede
{
    public class ManagerApp
    {
        public class Directory
        {
            public const string root = "_Centipede/";
            public const string resourcesRoot = root + "Resources/";
        }

        public class Resources
        {
            public const string root = "_Centipede/";
            public const string asset = root + "Asset/";

            public const string cell = asset + "Cell/";
            public const string enemy = asset + "Enemy/";
            public const string level = asset + "Level/";
            public const string player = asset + "Player/";
            public const string ui = asset + "UI/";
        }
    }
}
