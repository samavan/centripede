﻿using UnityEngine;
namespace Atari.Centripede
{
    public class ManagerMushroom
    {
        public static Texture2D [] GetLifeTextures(int life, Texture2D texture)
        {
            Texture2D[] textures = new Texture2D[life+1];
            textures[life] = texture;
            
            if(life <= 1)
            {
                return textures;
            }

            float rate = 1f / (life);

            Color emptyPixel = new Color(0, 0, 0, 0);

            int pixelEnd;
            int x, y;

            for(int i=1; i  < textures.Length - 1; i++)
            {
                textures[i] = new Texture2D(texture.width, texture.height);
                textures[i].filterMode = texture.filterMode;
                textures[i].wrapMode = texture.wrapMode;
                textures[i].SetPixels(texture.GetPixels());

                // remove pixels down to the area
                pixelEnd = (int)(texture.height * rate * i);
                
                for(y=0; y < texture.height - pixelEnd; y++)
                {
                    for (x = 0; x < texture.width; x++)
                    {
                        textures[i].SetPixel(x, y, emptyPixel);
                    }
                }
                textures[i].Apply();
            }

            return textures;
        }
    }
}
