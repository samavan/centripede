﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class PathFinding : MonoBehaviour
    {
        public bool debugMode = true;
        GameObject goDebug;
        List<GameObject> goDebugEnabled = new List<GameObject>();
        List<GameObject> goDebugDisabled = new List<GameObject>();

        void DrawPath(List<CellData> cells)
        {
            ResetDebug();

            foreach (CellData cell in cells)
            {
                AddGoDebug(cell.componentCell.gameObject.name, cell.componentCell.transform.position);
            }
        }

        void AddGoDebug(string name, Vector3 position)
        {
            if(!debugMode)
            {
                return;
            }

            GameObject go;
            if (goDebugDisabled.Count > 0)
            {
                go = goDebugDisabled[0];
                goDebugDisabled.Remove(go);
            }
            else
            {
                go = NewGoDebug(goDebug);
            }

            goDebugEnabled.Add(go);
            go.name = name;
            go.transform.position = position;
            go.SetActive(true);
        }

        void ResetDebug()
        {
            if(!debugMode)
            {
                return;
            }

            foreach (GameObject go in goDebugEnabled)
            {
                go.SetActive(false);
                goDebugDisabled.Add(go);
            }
            goDebugEnabled.Clear();
        }

        GameObject NewGoDebug(GameObject go)
        {
            go = Instantiate(go) as GameObject;
            go.transform.parent = transform;
            go.transform.localScale = goDebug.transform.localScale;
            return go;
        }

        void LogTrajectory(List<CellData> cells)
        {
            string log = "LogTrajectory()";
            foreach (CellData cell in cells)
            {
                log += "\n" + cell.componentCell.gameObject.name;
            }
            Debug.Log(log);
        }

        void LogAndDraw(List<CellData> cells)
        {
            LogTrajectory(cells);
            ResetDebug();
            DrawPath(cells);
        }

        public void OnDrawGizmosSelected()
        {
            if(pathFrom != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(
                pathFrom.cell.componentCell.transform.position - Vector3.forward * 0.0f,
                new Vector3(0.2f, 0.2f, 3));
            }

            if (pathTo != null)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawCube(
                pathTo.cell.componentCell.transform.position - Vector3.forward * 0.0f,
                new Vector3(0.2f, 0.2f, 3));
            }


            if (lastTrajectory != null && lastTrajectory.Count > 1)
            {
                for (int i=1; i < lastTrajectory.Count; i++)
                {
                    Gizmos.color = Color.Lerp(Color.blue, Color.red, i / (float)lastTrajectory.Count);
                    Gizmos.DrawLine(
                    lastTrajectory[i - 1].cell.componentCell.transform.position - Vector3.forward * 0.0f,
                    lastTrajectory[i].cell.componentCell.transform.position - Vector3.forward * 0.0f);
                }
            }

            return;
            if (newPathTested != null)
            {
                foreach (PathCellData path in masterPathDatas)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawSphere(
                    path.cell.componentCell.transform.position - Vector3.forward * 0.0f,
                    0.2f);

                }
            }

            if (masterPathDatas != null)
            {
                foreach(PathCellData path in masterPathDatas)
                {
                    
                    Gizmos.color = Color.magenta;
                    Gizmos.DrawLine(
                        path.cell.componentCell.transform.position + -Vector3.forward * -1,
                        path.cell.componentCell.transform.position + -Vector3.forward * path.fCost);

                    if (path.pathFrom != null)
                    {
                        Gizmos.color = Color.blue;
                        Gizmos.DrawLine(
                            path.cell.componentCell.transform.position + Vector3.forward * 0.0f,
                            path.pathFrom.cell.componentCell.transform.position + Vector3.forward * 0.5f);
                    }
                    foreach (PathCellData neighbor in path.neighbors)
                    {
                        Gizmos.color = Color.green;
                        Gizmos.DrawLine(
                            path.cell.componentCell.transform.position + Vector3.forward * 0.0f,
                            neighbor.cell.componentCell.transform.position + Vector3.forward * 0.5f);
                    }
                }
            }
        }
    }
}