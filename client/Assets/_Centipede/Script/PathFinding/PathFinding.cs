﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class PathFinding : MonoBehaviour
    {
        //[Serializable]
        public class PathCellData
        {
            public CellData cell;
            public Vector3 position;
            
            // coefs
            public float gCost; // distance from starting node
            public float hCost; // distance from end node
            public float fCost; // g + h;

            public PathCellData pathFrom;
            public PathCellData[] neighbors;
        }

        // Full Analysis result
        readonly List<PathCellData> masterPathDatas = new List<PathCellData>(); // clean path cell for final path

        // Temp Path Analysis Only
        readonly List<CellData> cellFound = new List<CellData>(); // do not loop in those cells
        readonly List<PathCellData> pathCellDataToBeTested = new List<PathCellData>(); // waiting list
        readonly List<PathCellData> newPathTested = new List<PathCellData>(); // path data already tested

        List<PathCellData> lastTrajectory = new List<PathCellData>(); // path data already tested

        PathCellData pathFrom;
        PathCellData pathTo;

        // to never block the game loop while master data analysis
        // search can take a while but other behavior should keep updating smoothly.
        readonly int frameNextTestDelay = 0;
        readonly int testPerFrame = 5; // a bit of loop per Update won't affect the update that much,
        float frameNextTest;

        // callback on completition
        public Action onPathDataReady;
        public Action<List<CellData>> onSuccess;
        public Action<List<CellData>> onFailed; // no way to the target be return closest path to...

        bool pathDataReady;
        bool isBusy;

        private void Start()
        {
            if(debugMode)
            {
                goDebug = GameObject.CreatePrimitive(PrimitiveType.Quad);
                goDebug.transform.parent = transform;
                goDebug.transform.localScale = new Vector3(0.2f, 0.2f, 1);
                goDebug.GetComponent<Renderer>().material.color = Color.red;
                goDebug.SetActive(false);
            }

            if (!isBusy)
            {
                State(false);
            }
        }

        private void LateUpdate()
        {
            UpdateAnalysis();
        }

        // to update the entire PathCellData
        public void GatherPathData(CellData cellFrom, CellData cellTo)
        {
            //Debug.Log("PathFinding.GatherPathData()");
            pathDataReady = false;

            Reset();
            cellFound.Clear();
            cellFound.Add(cellFrom);
            pathCellDataToBeTested.Add(NewPathCell(cellFrom, null));

            isBusy = true;
            State(true);
        }

        // new path from PathCellData available
        public void UpdatePathInfo(CellData cellFrom, CellData cellTo)
        {
            /*
            Debug.Log("PathFinding.UpdatePathInfo(" +
                cellFrom.componentCell.gameObject.name + ", " +
                cellTo.componentCell.gameObject.name + ")");
                */
           
            // find matching paths
            pathFrom = null;
            pathTo = null;

            foreach (PathCellData data in masterPathDatas)
            {
                if(data.cell == cellFrom)
                {
                    pathFrom = data;
                    break;
                }
            }

            foreach (PathCellData data in masterPathDatas)
            {
                if (data.cell == cellTo)
                {
                    pathTo = data;
                    break;
                }
            }
           
            if (pathTo == null)
            {
                int distance = 9999999;
                int tmp;

                //DebugLog("<color=red>pathTo no found.</color>");

                foreach (PathCellData path in masterPathDatas)
                {
                    tmp = (int)Vector3.Distance(path.cell.position, cellTo.position);
                    if(tmp < distance)
                    {
                        distance = tmp;
                        pathTo = path;
                    }
                }
            }
            /*
            DebugLog("pathFrom = " +
              pathFrom.cell.componentCell.gameObject.name + ", " +
              "pathTo = " +
              pathTo.cell.componentCell.gameObject.name + ")");
              */

            EvaluateEveryCost(pathFrom, pathTo);
            // EvaluateEveryCost(cellFrom.position, cellTo.position);
            onSuccess(GetPathFrom(pathFrom, pathTo));
        }

        void EvaluateEveryCost(PathCellData pathFrom, PathCellData pathTo)
        {
            pathCellDataToBeTested.Clear();
            pathCellDataToBeTested.Add(pathFrom);
            cellFound.Clear();
            cellFound.Add(pathFrom.cell);

            int security = 0;
            // G cost >> distance from START
            pathFrom.gCost = 0;
            while (pathCellDataToBeTested.Count > 0)
            {
                foreach (PathCellData path in pathCellDataToBeTested[0].neighbors)
                {
                    if(!cellFound.Contains(path.cell))
                    {
                        path.pathFrom = pathCellDataToBeTested[0];
                        path.gCost = path.pathFrom.gCost + 1;

                        pathCellDataToBeTested.Add(path);
                        cellFound.Add(path.cell);
                    }

                    /*
                    if (!pathCellDataToBeTested.Contains(path))
                    {
                        pathCellDataToBeTested.Add(path);
                    }
                    */
                }
                pathCellDataToBeTested.Remove(pathCellDataToBeTested[0]);

                security++;

                if (security > 1000)
                {
                    Debug.Log("exit from security");
                    break;
                }
            }

            // H cost >> distance from END
            pathTo.hCost = 0;
            pathCellDataToBeTested.Clear();
            pathCellDataToBeTested.Add(pathTo);
            cellFound.Clear();
            cellFound.Add(pathTo.cell);

            security = 0;

            while (pathCellDataToBeTested.Count > 0)
            {
                foreach (PathCellData path in pathCellDataToBeTested[0].neighbors)
                {
   
                    if (!cellFound.Contains(path.cell))
                    {
                        path.pathFrom = pathCellDataToBeTested[0];
                        path.hCost = path.pathFrom.hCost + 1;

                        pathCellDataToBeTested.Add(path);
                        cellFound.Add(path.cell);
                    }
                    /*
                    if (!pathCellDataToBeTested.Contains(path))
                    {
                        pathCellDataToBeTested.Add(path);
                    }
                    */
                }
                pathCellDataToBeTested.Remove(pathCellDataToBeTested[0]);

                security++;

                if (security > 1000)
                {
                    Debug.Log("exit from security");
                    break;
                }
            }

            // F cost >> G + H
            foreach (PathCellData path in masterPathDatas)
            {
                path.fCost = path.gCost + path.hCost;
                if(debugMode)
                {
                    path.cell.componentCell.debugG.text = path.gCost.ToString();
                    path.cell.componentCell.debugH.text = path.hCost.ToString();
                    path.cell.componentCell.debugF.text = path.fCost.ToString();
                }
            }
        }

        /*
        void EvaluateEveryCost(Vector3 posFrom, Vector3 posTo)
        {
            // sort List from center;

            foreach(PathCellData path in masterPathDatas)
            {
                EvaluateCost(path, posFrom, posTo);
            }
        }

        void EvaluateCost(PathCellData pathCell, Vector3 posFrom, Vector3 posTo)
        {

            pathCell.gCost = (float)System.Math.Round(Vector3.Distance(pathCell.position, posFrom), 2);
            pathCell.hCost = (float)System.Math.Round(Vector3.Distance(pathCell.position, posTo), 2);
            pathCell.fCost = pathCell.gCost + pathCell.hCost;

            if (debugMode)
            {
                pathCell.cell.componentCell.debugG.text = pathCell.gCost.ToString();
                pathCell.cell.componentCell.debugH.text = pathCell.hCost.ToString();
                pathCell.cell.componentCell.debugF.text = pathCell.fCost.ToString();
            }
        }
        */
        void UpdateAnalysis()
        {
            if (Time.frameCount >= frameNextTest)
            {
                for (int i = 0; i < testPerFrame; i++)
                {
                    CellAnalysis(pathCellDataToBeTested[0]);

                    if (pathCellDataToBeTested.Count == 0)
                    {
                        PathDataReady();
                        return;
                    }
                }
                frameNextTest = Time.time + frameNextTestDelay;
            }
        }

        void PathDataReady()
        {
            // DebugLog("<color=green>PathDataReady()</color>");
            pathDataReady = true;
            onPathDataReady();
            ResetDebug();
            State(false);
        }

        /*
        List<CellData> GetClosestPath(PathCellData pathFrom, CellData cellTarget, List<PathCellData> pathData)
        {
            Vector3 from = cellTarget.componentCell.transform.position;
            float tmpDistance;
            float distance = Mathf.Infinity;
            PathCellData closestPath = null;

            foreach(PathCellData path in pathData)
            {
                tmpDistance = Vector3.Distance(path.cell.componentCell.transform.position, from);
                if (tmpDistance < distance)
                {
                    distance = tmpDistance;
                    closestPath = path;
                }
            }

            return GetPathFrom(pathFrom, closestPath);
        }
        */

        List<CellData> GetPathFrom(PathCellData pathFrom, PathCellData pathTo)
        {
            List<CellData> cells = new List<CellData> { pathTo.cell };
            List<CellData> cellTested = new List<CellData> { pathTo.cell };
            PathCellData currentPath = pathTo;
            lastTrajectory = new List<PathCellData> { pathTo };
            float fCost;
            int secure=0;
            PathCellData closestPath= null;

            while (currentPath != null)
            {
                fCost = 99999999;

                foreach(PathCellData path in currentPath.neighbors)
                {
                    if(!cells.Contains(path.cell))
                    {
                        if (path.fCost < fCost)
                        {
                            fCost = path.fCost;
                            closestPath = path;
                        }
                        
                        else if (path.fCost == fCost)
                        {
                            if (path.gCost < closestPath.gCost)
                            {
                                closestPath = path;
                            }
                        }                  
                    }
                    cellTested.Add(path.cell);
                }
                lastTrajectory.Add(closestPath);
                currentPath = closestPath;
               
                if(cells.Contains(currentPath.cell))
                {
                    // DebugLog("<color=magenta>left loop</color> ( found in cells)");
                    break;
                }

                if (currentPath.cell == pathFrom.cell)
                {
                    // DebugLog("<color=magenta>left loop</color> (pathFrom crossed)");
                    break;
                }

                cells.Add(currentPath.cell);

                secure++;

                if (secure > masterPathDatas.Count)
                {
                    // DebugLog("<color=red>Too much loop</color>");
                    break;
                }
            }

            // path starts from end then to flip result
            cells.Reverse();

            if (debugMode)
            {
                //DebugLog(" masterPathDatas.Count = " + masterPathDatas.Count);
                LogAndDraw(cells);
            }

            return cells;
        }

        void CellAnalysis(PathCellData pathCell)
        {
            AddGoDebug(pathCell.cell.componentCell.gameObject.name, pathCell.cell.componentCell.transform.position);

            pathCell.neighbors = GetNoTestedNeighbors(pathCell).ToArray();
            pathCell.position = pathCell.cell.position;

            // those cells won't be returned anymore;
            foreach (PathCellData path in pathCell.neighbors)
            {
                if(!cellFound.Contains(path.cell))
                {
                    cellFound.Add(path.cell);
                    pathCellDataToBeTested.Add(path);
                }
            }

//            pathCellDataToBeTested.AddRange(pathCell.pathNeighbors);
            masterPathDatas.Add(pathCell);

            // current cell won't be tested anymore
            pathCellDataToBeTested.Remove(pathCell);
        }

        List<PathCellData> tmpPathCells = new List<PathCellData>(); //to avoid re-declaration/allocation

        List<PathCellData> GetNoTestedNeighbors(PathCellData pathCell)
        {
            tmpPathCells.Clear();

            if (IsCellAvailable(pathCell.cell.cellUp))
            {
                tmpPathCells.Add(GetPathCellData(pathCell.cell.cellUp, pathCell));
            }

            if (IsCellAvailable(pathCell.cell.cellDown))
            {
                tmpPathCells.Add(GetPathCellData(pathCell.cell.cellDown, pathCell));
            }

            if (IsCellAvailable(pathCell.cell.cellLeft))
            {
                tmpPathCells.Add(GetPathCellData(pathCell.cell.cellLeft, pathCell));
            }

            if (IsCellAvailable(pathCell.cell.cellRight))
            {
                tmpPathCells.Add(GetPathCellData(pathCell.cell.cellRight, pathCell));
            }

            return tmpPathCells;
        }

        PathCellData GetPathCellData(CellData cell, PathCellData pathFrom)
        {
            foreach(PathCellData path in masterPathDatas)
            {
                if (path.cell == cell)
                {
                    // DebugLog("<color=white> FOUND : " + cell.componentCell.name + "</color>");
                    return path;
                }
            }

            foreach (PathCellData path in pathCellDataToBeTested)
            {
                if (path.cell == cell)
                {
                    // DebugLog("<color=white> FOUND : " + cell.componentCell.name + "</color>");
                    return path;
                }
            }

            // DebugLog("<color=grey> NOT FOUND : " + cell.componentCell.name + "</color>");
            return NewPathCell(cell, pathFrom);
        }

        bool IsCellAvailable(CellData cell)
        {
            return cell != null  
                //&& !cellFound.Contains(cell)        // not found yet
                && !cell.componentCell.IsVisible(); // not a murshroom
        }

        PathCellData NewPathCell(CellData cell, PathCellData pathFrom)
        {
            return new PathCellData
            {
                cell = cell,
                pathFrom = pathFrom
            };
        }

        public bool DataReady()
        {
            return pathDataReady;
        }

        public void Reset()
        {
            ResetDebug();
            cellFound.Clear();
            pathCellDataToBeTested.Clear();
            masterPathDatas.Clear();
        }

        public void State(bool state)
        {
            // DebugLog(".State(" + state + ");");
            enabled = state;
        }

        void DebugLog(object log)
        {
            Debug.Log(gameObject.name +  " : " + log + ";");
        }
    }
}