﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class PathFindingFactory : Singleton<PathFindingFactory>
    {
        List<PathFinding> paths = new List<PathFinding>();

        public PathFinding New(string baseName)
        {
            PathFinding path = new GameObject("path-" + baseName).AddComponent<PathFinding>();
            path.transform.parent = transform;
            path.transform.Reset();
            paths.Add(path);
            return path;
        }

        public void Reset()
        {
            foreach(PathFinding path in paths)
            {
                path.Reset();
            }
        }
    }
}