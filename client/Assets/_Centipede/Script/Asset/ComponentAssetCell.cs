﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public class ComponentAssetCell : ComponentAsset
    {
        public GameObject goDebug;
        public TextMesh debugG;
        public TextMesh debugH;
        public TextMesh debugF;
        public TextMesh debugIdx;

        //[HideInInspector]
        public CellData data;
        public GameObject goHit;

        public Texture2D[] lifeTextures;
        Material mat;

        public void State(bool state)
        {
            goDebug.SetActive(false);
            goGraphic.SetActive(state);
            goDeath.SetActive(false);
            goHit.SetActive(false);
        }

        public override void Hit(byte damage)
        {
            base.Hit(damage);
            goHit.SetActive(false);
            goHit.SetActive(true);
            SetLifeTexture();
        }

        public override void Death()
        {
            base.Death();
            sApp.Instance.OnScoreAdd(1);
        }

        public bool IsVisible()
        {
            return goGraphic.activeSelf;
        }

        public void Init(Vector3 position)
        {
            transform.position = position;
            data.componentEntity = new List<ComponentAsset>();
            SetLifeTexture();
        }

        void SetLifeTexture()
        {
            if(!mat)
            {
                mat = goGraphic.GetComponentInChildren<Renderer>(true).material;
            }
            mat.mainTexture = lifeTextures[life];
        }

        public void OnDrawGizmosSelected()
        {
            /*
            if (data.componentEntity != null)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, data.componentEntity.transform.position + Vector3.forward);
            }
            */

            if (data.cellLeft != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, data.cellLeft.componentCell.transform.position + Vector3.forward);
            }

            if (data.cellRight != null)
            {
                Gizmos.color = Color.magenta;
                Gizmos.DrawLine(transform.position, data.cellRight.componentCell.transform.position + Vector3.forward);
            }

            if (data.cellUp != null)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, data.cellUp.componentCell.transform.position + Vector3.forward);
            }

            if (data.cellDown != null)
            {
                Gizmos.color = Color.cyan;
                Gizmos.DrawLine(transform.position, data.cellDown.componentCell.transform.position + Vector3.forward);
            }

        }
    }
}
