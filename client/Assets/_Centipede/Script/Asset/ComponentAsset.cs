﻿using UnityEngine;

namespace Atari.Centripede
{
    public class ComponentAsset : MonoBehaviour
    {
        public GameObject goGraphic;
        protected Transform trGraphic;
        public GameObject goDeath;

        //[HideInInspector]
        public byte life;

        public virtual void Hit(byte damage)
        {
            life -= damage;
            if (life <= 0)
            {
                Death();
            }
        }

        public virtual void Death()
        {
            goGraphic.SetActive(false);
            goDeath.SetActive(true);
        }
    }
}
