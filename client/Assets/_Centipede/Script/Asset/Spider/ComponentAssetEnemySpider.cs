﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemySpider : ComponentAsset
    {
        // data default
        byte spawnDelay;
        byte speed;
        Action onDeath;

        // graphic
        public GameObject goDefaultPosition;
        public GameObject goTrajectoryTemplate;
        Transform trTrajectoryRoot;

        // cell tests
        CellMove cellMove;

        CellData homeCell; // spider starts from here : default cell
        CellData cell;  
        CellData cellHistory;

        // to avoid re-declaration while path finding test
        ComponentAsset entityTarget;

        //  to manage spider behavior
        public bool doMove;

        PathFinding pathFinding;


        private void LateUpdate()
        {
            if (behavior == SPIDER_BEHAVIOR.GO_TO_TARGET &&
                playerUpperCell != sApp.Instance.PlayerUpperCell())
            {
                UpdatePathToPlayer();
            }

            if (doMove)
            {
                transform.position = cellMove.GoTo();
            }
        }

        public void Init(CellData cell, byte speed, byte spawnDelay, Action onDeath = null)
        {
            goTrajectoryTemplate.SetActive(false);
            if (cellMove == null)
            {
                cellMove = new CellMove();
                cellMove.onMoveEnd = OnMovedToCell;

                goDefaultPosition.transform.parent = EnemyFactory.Instance.transform;

                trGraphic = goGraphic.transform;
                trTrajectoryRoot = new GameObject("_Trajectory-Spider").transform;
                trTrajectoryRoot.parent = EnemyFactory.Instance.transform;
                trTrajectoryRoot.Reset();

                pathFinding = PathFindingFactory.Instance.New("Spider");
                pathFinding.onPathDataReady = OnPathDataReady;
                pathFinding.onSuccess = OnPathFound;
                pathFinding.onFailed = OnNoPathFound;
                pathFinding.debugMode = debugTrajectory;
            }

            goDefaultPosition.transform.position = cell.position;

            homeCell = cell;
            this.cell = homeCell;
            this.onDeath = onDeath;
            this.speed = speed;
            this.spawnDelay = spawnDelay;
            behavior = SPIDER_BEHAVIOR.GO_TO_TARGET;
            gameObject.SetActive(false);
            EvaluateFullPath(homeCell, sApp.Instance.PlayerUpperCell());
            ResetTrajectoryView();
        }

        public void Spawn()
        {
            // dirty reset effect
            gameObject.SetActive(false);
            gameObject.SetActive(true);

            goGraphic.SetActive(true);
            goDeath.SetActive(false);

            MoveState(false);
            behavior = SPIDER_BEHAVIOR.WAITING_AT_HOME;
            cell = homeCell;
            cell.componentEntity.Add(this);
            cellHistory = null;
            transform.position = cell.componentCell.transform.position;
            EvaluateFullPath(cell, sApp.Instance.PlayerUpperCell());
            StartCoroutine(WaitForHunting());
            enabled = true;
        }

        void MoveState(bool state)
        {
            //DebugLog("MoveState(" + state + ")");
            doMove = state;
        }

        public void State(bool state)
        {
            goGraphic.SetActive(state);
            goDeath.SetActive(false);
            enabled = false;
        }

    }
}