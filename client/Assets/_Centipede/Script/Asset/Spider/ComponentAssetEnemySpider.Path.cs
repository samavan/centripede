﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemySpider : ComponentAsset
    {
        // path from pathfinding
        List<CellData> path;
        CellData pathCellTarget;

        CellData playerUpperCell;

        void EvaluateFullPath(CellData from, CellData to)
        {
            playerUpperCell = sApp.Instance.PlayerUpperCell();
            pathFinding.GatherPathData(from, to);
        }

        void UpdatePathToPlayer()
        {
            playerUpperCell = sApp.Instance.PlayerUpperCell();
            RegisterLog("pathFinding.UpdatePathInfo(" + cell + ", " + playerUpperCell + ")");
            pathFinding.UpdatePathInfo(cell, playerUpperCell);
            return;

            try
            {
                playerUpperCell = sApp.Instance.PlayerUpperCell();
                RegisterLog("pathFinding.UpdatePathInfo(" + cell + ", " + playerUpperCell + ")");
                pathFinding.UpdatePathInfo(cell, playerUpperCell);
            }
            catch
            {
                Debug.LogError("UpdatePathToPlayer() error catch : \n" + cellSearchLog + ";");
            }           
        }

        void NextCellSearch()
        {
            if(cell != null)
            {
                cell.componentEntity.Remove(this);
            }

            cellHistory = cell;
            cellSearchLog = "";

            switch (behavior)
            {
                case SPIDER_BEHAVIOR.GO_TO_HOME:
                    if (cell == homeCell)
                    {
                        RegisterLog("Back To default position");
                        IsBackHome();
                        return;
                    }
                    break;
                case SPIDER_BEHAVIOR.GO_TO_TARGET:
                    if (cell == sApp.Instance.PlayerUpperCell())
                    {
                        RegisterLog("Player Upper cell reached >> ready to kill");
                        ReadyToKill();
                        return;
                    }
                    else if (cell == pathCellTarget)
                    {
                        RegisterLog("Didn't reach the player upper cell but end of path");
                        ForceGoBackHome();
                        return;
                    }
                    break;
            }

            RegisterLog("Reached Cell (" + behavior + ")");
            
            switch(behavior)
            {
                case SPIDER_BEHAVIOR.GO_TO_HOME:
                    cell = path[0];
                    path.Remove(cell);
                    break;
                case SPIDER_BEHAVIOR.GO_TO_TARGET:
                    if (playerUpperCell != sApp.Instance.PlayerUpperCell())
                    {
                        UpdatePathToPlayer();
                    }
                    /*
                    if (path.Count == 0) // a possible update of the path
                    {
                        return;
                    }    
                    */
                    cell = path[0];
                    path.Remove(cell);
                    break;
            }
            GoToCell(cell);
        }

        void GoToCell(CellData cell)
        {
            this.cell = cell;

            //if (logBehavior && cellSearchLog != "") { Debug.Log(cellSearchLog); }

            cell.componentEntity.Add(this);
            cellMove.Init(speed, cellHistory, cell);
            DrawTrajectory(cell.position);

            if (!doMove)
            {
                MoveState(true);
            }
        }
    }
}