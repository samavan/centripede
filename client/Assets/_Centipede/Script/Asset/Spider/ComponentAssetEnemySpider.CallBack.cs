﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemySpider : ComponentAsset
    {
        void OnPathDataReady()
        {
            RegisterLog("OnPathDataReady()");
            UpdatePathToPlayer();
            ReadyToGo();
        }

        void OnPathFound(List<CellData> cells)
        {
            RegisterLog("OnPathFound()");
            SetPath(cells);
        }

        void OnNoPathFound(List<CellData> cells)
        {
            RegisterLog("OnNoPathFound()");
            // no path found but keep moving to shortest spot
            SetPath(cells);
        }

        void SetPath(List<CellData> cells)
        {
            // to always remove current cell
            cells.Remove(cell);
            //cells.Remove(cellHistory);

            path = cells;
            pathCellTarget = cells[cells.Count - 1];

            if(!doMove || !cellMove.isBusy)
            {
                NextCellSearch();
            }
        }

        void OnMovedToCell()
        {
            if(behavior != SPIDER_BEHAVIOR.GO_TO_HOME 
                && behavior != SPIDER_BEHAVIOR.GO_TO_TARGET)
            {
                return;
            }

            try
            {
                NextCellSearch();
            }
            catch
            {
                Debug.LogError("The following happened while NextCellSearch : \n" + cellSearchLog + ";");
            }
        }
    }
}