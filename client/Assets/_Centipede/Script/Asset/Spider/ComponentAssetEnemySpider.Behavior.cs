﻿using System.Collections;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemySpider : ComponentAsset
    {
        public enum SPIDER_BEHAVIOR
        {
            DEAD,
            WAITING_AT_HOME,
            GO_TO_TARGET,
            REACHED_TARGET,
            GO_TO_HOME
        }

        public SPIDER_BEHAVIOR behavior;
        bool readyToHunt;

        IEnumerator WaitForHunting()
        {
            readyToHunt = false;
            RegisterLog("WaitForPlayerHunting");
            yield return new WaitForSeconds(2);
            readyToHunt = true;
            ReadyToHunt();
        }

        IEnumerator WaitToGoBackHome()
        {
            yield return new WaitForSeconds(1);
            RegisterLog("On the way back");
            NextCellSearch();
        }

        IEnumerator WaitToKillEntity()
        {
            yield return new WaitForSeconds(0.5f);
            // to check if the player is still there
            entityTarget = ManagerGrid.CheckForPlayerInCell(cell.cellDown);
            if (entityTarget)
            {
                //RegisterLog("Successs Kill");
                MoveState(false);
                entityTarget.Hit(1);
            }
            else
            {
                MoveState(false);
                //RegisterLog("Failed Kill > WaitToGoBack()");
                ForceGoBackHome();
            }
        }

        IEnumerator WaitForReSpawn()
        {
            yield return new WaitForSeconds(spawnDelay);
            Spawn();
        }

        void IsBackHome()
        {
            behavior = SPIDER_BEHAVIOR.WAITING_AT_HOME;
            MoveState(false);
            ResetTrajectoryView();
            StartCoroutine(WaitForHunting());

            // to always evaluate path data once back home
            // >> there are almost always blocks destroyed by the player.

            path = null;
            EvaluateFullPath(homeCell, sApp.Instance.PlayerUpperCell());
        }

        void ReadyToKill()
        {
            MoveState(false);
            behavior = SPIDER_BEHAVIOR.REACHED_TARGET;
            StartCoroutine(WaitToKillEntity());
        }

        void ForceGoBackHome()
        {
            RegisterLog("ForceGoBackHome()");
            behavior = SPIDER_BEHAVIOR.GO_TO_HOME;
            ResetTrajectoryView();
            pathFinding.UpdatePathInfo(cell, homeCell);
            MoveState(false);
            StartCoroutine(WaitToGoBackHome());
        }
        
        void ReadyToGo()
        {
            // path Data are ready
            switch(behavior)
            {
                case SPIDER_BEHAVIOR.WAITING_AT_HOME:
                    ReadyToHunt();
                    break;
                case SPIDER_BEHAVIOR.REACHED_TARGET:
                    ForceGoBackHome();
                    break;
            }
        }

        void ReadyToHunt()
        {
            if (behavior == SPIDER_BEHAVIOR.DEAD ||
                behavior == SPIDER_BEHAVIOR.GO_TO_TARGET
                || !readyToHunt 
                || !pathFinding.DataReady())
            {
                RegisterLog("Not Ready for Hunting : " + readyToHunt + ", " + pathFinding.DataReady() + ";");
                return;
            }

            behavior = SPIDER_BEHAVIOR.GO_TO_TARGET;
            UpdatePathToPlayer();
        }

        public override void Hit(byte damage)
        {
            Death();
        }

        public override void Death()
        {
            base.Death();
            behavior = SPIDER_BEHAVIOR.DEAD;
            pathFinding.State(false);
            pathFinding.Reset();
            MoveState(false);
            cell.componentEntity.Remove(this);
            onDeath();
            ResetTrajectoryView();
            StartCoroutine(WaitForReSpawn());
            enabled = false;
        }
    }
}