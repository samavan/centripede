﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemySpider : ComponentAsset
    {
        readonly bool logBehavior = false;
        string cellSearchLog = "";
        public bool debugTrajectory;
        readonly List<GameObject> goTrajectories = new List<GameObject>();

        void DrawTrajectory(Vector3 position)
        {
            if(!debugTrajectory)
            {
                return;
            }

            GameObject go = null;
            foreach (GameObject _go in goTrajectories)
            {
                if (!_go.activeSelf)
                {
                    go = _go;
                    break;
                }
            }

            if (!go)
            {
                go = Instantiate(goTrajectoryTemplate) as GameObject;
                go.name = "Trajectory_" + goTrajectories.Count;
                go.transform.parent = trTrajectoryRoot;
                goTrajectories.Add(go);
            }

            go.transform.Reset();
            go.transform.position = position;
            go.SetActive(true);
        }

        void DrawTrajectory(List<CellData> cells)
        {
            ResetTrajectoryView();
            foreach(CellData cell in cells)
            {
                DrawTrajectory(cell.componentCell.transform.position);
            }
        }
        
        void ResetTrajectoryView()
        {
            foreach (GameObject go in goTrajectories)
            {
                go.SetActive(false);
            }
        }

        public void OnDrawGizmosSelected()
        {
            if (cell != null)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position + Vector3.forward * 0.2f, sApp.Instance.PlayerUpperCell().position + Vector3.forward * 0.2f);
            }
        }

        void RegisterLog(object log)
        {
            if (!logBehavior)
            {
                return;
            }
            int countLog = cellSearchLog.Split('\n').Length - cellSearchLog.Split('\t').Length;
            cellSearchLog += "\n-" + countLog + "-" + log + ";";
        }
    }
}