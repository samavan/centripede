﻿using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemyCentipede : ComponentAsset
    {
        bool logBehavior = false;

        string cellSearchLog;

        public void OnDrawGizmosSelected()
        {
            if (nextPart)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(transform.position, nextPart.transform.position - Vector3.forward * 0.5f);
            }

            if (cell != null && cell.componentCell)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, cell.componentCell.transform.position + Vector3.forward * 0.5f);
            }
        }

        void RegisterLog(string log)
        {
            if(!logBehavior)
            {
                return;
            }
            int countLog = cellSearchLog.Split('\n').Length - cellSearchLog.Split('\t').Length;
            cellSearchLog += "\n-" + countLog + "-" + log + ";";
        }

        void DebugLog(string log)
        {
            return;
            Debug.Log("Centiped : " + log + ";");
        }
    }
}
