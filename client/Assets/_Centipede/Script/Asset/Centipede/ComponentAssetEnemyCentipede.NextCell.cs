﻿using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemyCentipede : ComponentAsset
    {
        bool stuckOnFloor;

        void NextCellSearch()
        {
            cell.componentEntity.Remove(this);
            cellSearchLog = "";
            cellTmp = null;

            RegisterLog("Next direction : " + direction);

            // could be cut tail and became a new HEAD
            if (cellForceOppositeDirection == cell)
            {
                RegisterLog("Special cell : force opposite direction (" + cellForceOppositeDirection.x + "," + cellForceOppositeDirection.y + ")");
                cellForceOppositeDirection = null;
                direction = OppositeDirection(direction);
                cellTmp = GetNextCell(cell, direction);
            }
            else
            {
                if (direction == ENTITY_DIRECTION.DOWN || direction == ENTITY_DIRECTION.UP)
                {
                    ArrivedToNewFloor();
                }

                if (forceNextFloorOnFreeCell)
                {
                    ForceToNextFloor();
                }
            }

            if (cellTmp == null)
            {
                cellTmp = GetNextCell(cell, direction);
            }

            if (cellTmp == null)
            {
                CellStuckResolve();
            }

            if (logBehavior && cellSearchLog != "") {Debug.Log(cellSearchLog);}

            cellHistory = cell;
            cell = cellTmp;
            
            // to test if player is there
            entityFound = ManagerGrid.CheckForPlayerInCell(cell);
            if (entityFound)
            {
                entityFound.Hit(1);
                return;
            }
            
            // otherwise just keep going
            cell.componentEntity.Add(this);
            cellMove.Init(speed, cellHistory, cell);
            SetGraphicDirection(direction);
        }

        void ForceToNextFloor()
        {
            RegisterLog("Been forced to go " + directionFloor + " on free cell");
            cellTmp = GetNextCell(cell, directionFloor);

            if (cellTmp != null)
            {
                forceNextFloorOnFreeCell = false;
                direction = directionFloor;
                RegisterLog("Cell available then apply new direction : " + direction);
                return;
            }

            RegisterLog("Not possible then keep going to " + direction);
            cellTmp = GetNextCell(cell, direction);
            //cellLoopHistory.Clear();
            //if (cellTmp != null)
            //{
            //    cellLoopHistory.Add(cellTmp);
            //    RegisterLog("Cell available to the " + direction);
            //}

            forceNextFloorOnFreeCell = cellTmp == null;
        }
        
        void ArrivedToNewFloor()
        {
            RegisterLog("Arrived new floor");
            stuckOnFloor = false;
            direction = OppositeDirection(directionHistory);
            RegisterLog("New direction : " + direction);
            cellTmp = GetNextCell(cell, direction);

            if (cellTmp != null)
            {
                RegisterLog("Free Cell found");
                return;
            }
            
            RegisterLog("Couldn't go " + direction);
            direction = OppositeDirection(direction);
            RegisterLog("New direction : " + direction);
            cellTmp = GetNextCell(cell, direction);

            if (cellTmp != null)
            {
                RegisterLog("Free Cell found");
                return;
            }

            RegisterLog("Couldn't go " + direction + " > stuck on both side.");
            direction = directionFloor;
            RegisterLog("New direction : " + direction);
            cellTmp = GetNextCell(cell, direction);

            if (cellTmp != null)
            {
                RegisterLog("Free Cell found");
                return;
            }

            RegisterLog("Couldn't go " + direction + " > stuck in a U shape.");
            directionFloor = OppositeDirection(directionFloor);
            RegisterLog("New direction Floor : " + directionFloor);
            direction = directionFloor;
            RegisterLog("New direction : " + direction);
            cellTmp = GetNextCell(cell, direction);

            if (cellTmp != null)
            {
                RegisterLog("Free Cell found");
                return;
            }
        }

        void CellStuckResolve()
        {
            RegisterLog("CellStuckResolve()");

            if (cellTmp == null)
            {
                RegisterLog(cell.componentCell.name + "(" + cell.x + "," + cell.y + ")");

                if (sApp.Instance.defaultGameData.gridWidth == 0 && cell.y == 0)
                {
                    RegisterLog("Reached Bottom END");
                    direction = OppositeDirection(direction);
                    RegisterLog("New direction : " + direction);
                    directionFloor = ENTITY_DIRECTION.UP;
                    RegisterLog("New direction Floor : " + directionFloor);
                    cellTmp = GetNextCell(cell, direction);
                }
                else if (cell.x == 0
                    && cell.y == sApp.Instance.defaultGameData.gridHeight - 1)
                {
                    RegisterLog("Reached TOP END");
                    direction = OppositeDirection(direction);
                    RegisterLog("New direction : " + direction);
                    directionFloor = ENTITY_DIRECTION.DOWN;
                    RegisterLog("New direction Floor : " + directionFloor);
                    cellTmp = GetNextCell(cell, direction);
                }

                if (cellStuckFrom == cell)
                {
                    RegisterLog("Not stuck but looping on it");
                    directionFloor = OppositeDirection(directionFloor);
                    RegisterLog("New direction Floor : " + directionFloor);
                    cellTmp = GetNextCell(cell, directionFloor);
                    cellStuckFrom = null;
                }

                if (cellTmp == null)
                {
                    if (cell.y == 0
                        || cell.y == sApp.Instance.defaultGameData.gridHeight - 1
                        && cellHistory == cell)
                    {
                        RegisterLog("Reach last line : seems stuck : " + cell.componentCell.name + "(" + cell.x + "," + cell.y + ")");
                        directionFloor = OppositeDirection(directionFloor);
                        RegisterLog("New direction Floor : " + directionFloor);
                        directionHistory = direction;
                        direction = directionFloor;
                        RegisterLog("New direction  : " + direction);
                        cellTmp = GetNextCell(cell, direction);
                        cellStuckFrom = null; // should be reset each time with go to new floor

                        RegisterLog("Still cannot go " + direction +" then back way");
                        if (cellTmp == null)
                        {
                            direction = OppositeDirection(directionHistory);
                            cellTmp = GetNextCell(cell, direction);

                            if (stuckOnFloor)
                            {
                                RegisterLog("forceLeadDirectonOnFreeCell ALREADY true");
                                directionFloor = OppositeDirection(directionFloor);
                                RegisterLog("New directionFloor : " + directionFloor);
                            }
                            else
                            {
                                RegisterLog("forceLeadDirectonOnFreeCell = true");
                                forceNextFloorOnFreeCell = true;
                                stuckOnFloor = true;
                            }
                        }
                    }

                    if (cellTmp == null)
                    {
                        if (cellStuckFrom != null)
                        {
                            RegisterLog("backuping stuck cell : " + cell.componentCell.name + "(" + cell.x + "," + cell.y + ")");
                            cellStuckFrom = cell;
                        }

                        RegisterLog("can't go further : " + cell.componentCell.name + "(" + cell.x + ", " + cell.y + ")" +
                            "\n\t>> go to next floor : " + directionFloor);

                        directionHistory = direction;
                        direction = directionFloor;
                        RegisterLog("New direction : " + direction);
                        cellTmp = GetNextCell(cell, direction);

                        if (cellTmp == null)
                        {
                            RegisterLog("Possible L shape stuck : cannot go to next floor (" + directionFloor + ")");

                            if (stuckOnFloor)
                            {
                                RegisterLog("forceLeadDirectonOnFreeCell ALREADY true");
                                directionFloor = OppositeDirection(directionFloor);
                                RegisterLog("New directionFloor : " + directionFloor);
                            }
                            else
                            {
                                RegisterLog("forceLeadDirectonOnFreeCell = true");
                                forceNextFloorOnFreeCell = true;
                                stuckOnFloor = true;

                            }
                            direction = OppositeDirection(directionHistory);
                            RegisterLog("New direction : " + direction);
                            cellTmp = GetNextCell(cell, direction);

                            if (cellTmp == null)
                            {
                                RegisterLog("Possible U shape stuck");
                                direction = OppositeDirection(directionFloor);
                                RegisterLog("New direction : " + direction);
                                cellTmp = GetNextCell(cell, direction);

                                if (cellTmp == null)
                                {
                                    RegisterLog("Unknow stuck Possibility");
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                RegisterLog("Next Cell found : " + cell.componentCell.name + "(" + cell.x + "," + cell.y + ")");
            }
        }
    }
}
