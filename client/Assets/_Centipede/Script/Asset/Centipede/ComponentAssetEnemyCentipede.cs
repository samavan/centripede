﻿using System;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class ComponentAssetEnemyCentipede : ComponentAsset
    {
        public Texture texHead;
        public Texture texBody;

        public CellData cell;           // current cell
        public CellData cellHistory;    // previousCell
        public CellData cellStuckFrom;  // to catch a dead end loop
        public CellData cellForceOppositeDirection; // should forcer opposite direction once reached;
        public CellData cellTmp;        // to avoid declaration on each loop

        CellMove cellMove;
        ComponentAsset entityFound;
        bool forceNextFloorOnFreeCell;

        byte speed;
        Material material;              // to switch texture (body or head)
        Action onDeath;                 // mostly testing if centipede is entirely dead
        CENTIPEDE_BODYPART bodyPart;    // body or head

        ENTITY_DIRECTION direction;     // left or right
        ENTITY_DIRECTION directionHistory;
        ENTITY_DIRECTION directionFloor; // up or down

        // centipede chain
        [HideInInspector]
        public ComponentAssetEnemyCentipede nextPart;
        [HideInInspector]
        public ComponentAssetEnemyCentipede previousPart;

        private void LateUpdate()
        {
            transform.position = cellMove.GoTo();
        }

        void OnMovedToCell()
        {
            NextCellSearch();
            return;

            try
            {
                NextCellSearch();
            }
            catch
            {
                Debug.LogError("The following happened while looking for free cell : \n" + cellSearchLog + ";");
            }
        }

        ENTITY_DIRECTION OppositeDirection(ENTITY_DIRECTION dir)
        {
            if (dir == ENTITY_DIRECTION.LEFT ||
                dir == ENTITY_DIRECTION.RIGHT)
            {
                return dir == ENTITY_DIRECTION.LEFT ? ENTITY_DIRECTION.RIGHT : ENTITY_DIRECTION.LEFT;
            }
            else if (dir == ENTITY_DIRECTION.UP ||
                dir == ENTITY_DIRECTION.DOWN)
            {
                return dir == ENTITY_DIRECTION.UP ? ENTITY_DIRECTION.DOWN : ENTITY_DIRECTION.UP;
            }

            // in case dir == ENTITY_DIRECTION.NONE
            return ENTITY_DIRECTION.DOWN;
        }

        CellData GetNextCell(CellData cell, ENTITY_DIRECTION dir)
        {
            switch (dir)
            {
                case ENTITY_DIRECTION.LEFT:
                    if (cell.cellLeft == null || cell.cellLeft.componentCell.IsVisible())
                    {
                        break;
                    }
                    return cell.cellLeft;
                case ENTITY_DIRECTION.RIGHT:
                    if (cell.cellRight == null || cell.cellRight.componentCell.IsVisible())
                    {
                        break;
                    }
                    return cell.cellRight;
                case ENTITY_DIRECTION.UP:

                    if (cell.cellUp == null || cell.cellUp.componentCell.IsVisible())
                    {
                        break;
                    }
                    return cell.cellUp;
                case ENTITY_DIRECTION.DOWN:
                    if (cell.cellDown == null || cell.cellDown.componentCell.IsVisible())
                    {
                        break;
                    }
                    return cell.cellDown;
            }
            return null;
        }

        void SetGraphicDirection(ENTITY_DIRECTION dir)
        {
            switch (dir)
            {
                case ENTITY_DIRECTION.LEFT:
                    trGraphic.localEulerAngles = new Vector3(0, 0, 90);
                    break;
                case ENTITY_DIRECTION.RIGHT:
                    trGraphic.localEulerAngles = new Vector3(0, 0, -90);
                    break;
                case ENTITY_DIRECTION.UP:
                    trGraphic.localEulerAngles = Vector3.zero;
                    break;
                case ENTITY_DIRECTION.DOWN:
                    trGraphic.localEulerAngles = new Vector3(0, 0, 180);
                    break;
            }
        }

        public void Init(CellData cell, byte speed, Action onDeath = null)
        {
            if (cellMove == null)
            {
                cellMove = new CellMove();
                cellMove.onMoveEnd = OnMovedToCell;

                trGraphic = goGraphic.transform;
            }

            this.onDeath = onDeath;
            this.cell = cell;
            this.speed = speed;
            transform.position = cell.componentCell.transform.position;
            direction = ENTITY_DIRECTION.RIGHT;
            directionFloor = ENTITY_DIRECTION.DOWN;
            OnMovedToCell();

            goGraphic.SetActive(true);
            goDeath.SetActive(false);
            enabled = false;
        }

        public override void Hit(byte value)
        {
            Death();
        }

        public override void Death()
        {
            base.Death();

            if(previousPart)
            {
                previousPart.nextPart = null;
            }

            if (nextPart)
            {
                nextPart.SetBodyPart(CENTIPEDE_BODYPART.HEAD);
                nextPart.ForceOppositeDirection(cell);
            }

            cell.componentEntity.Remove(this);
            enabled = false;
            onDeath();
        }

        public void ForceOppositeDirection(CellData cell)
        {
            cellForceOppositeDirection = cell;
            if (nextPart)
            {
                nextPart.ForceOppositeDirection(cell);
            }
        }

        public void SetBodyPart(CENTIPEDE_BODYPART bodyPart)
        {
            this.bodyPart = bodyPart;

            if(!material)
            {
                material = goGraphic.GetComponentInChildren<Renderer>(true).material;
            }

            switch (bodyPart)
            {
                case CENTIPEDE_BODYPART.BODY:
                    material.mainTexture = texBody;
                    return;
                case CENTIPEDE_BODYPART.HEAD:
                    material.mainTexture = texHead;
                    break;
            }
        }

        public void State(bool state)
        {
            goGraphic.SetActive(state);
            goDeath.SetActive(false);
            enabled = true;
        }
    }
}
