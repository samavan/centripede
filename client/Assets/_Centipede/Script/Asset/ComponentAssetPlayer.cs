﻿using System;
using UnityEngine;

namespace Atari.Centripede
{
    public class ComponentAssetPlayer : ComponentAsset
    {
        PlayerData data;

        CellData cell; //always keep track where the player is located.
        CellMove cellMove;

        // player Move
        bool goTo;
        Action onMoveEnd;

        // shoot
        public GameObject goProjectile;
        ComponentAssetProjectile projectile;
        Action onShootEnd;

        Action onDeath;

        private void LateUpdate()
        {
            if(goTo)
            {
                transform.position = cellMove.GoTo();
            }    
        }

        public bool MoveTo(ENTITY_DIRECTION dir, Action onMoveEnd)
        {
            cell.componentEntity.Remove(this);
            CellData cellTo = null;

            switch (dir)
            {
                case ENTITY_DIRECTION.LEFT:
                    if(cell.cellLeft == null || cell.cellLeft.componentCell.IsVisible())
                    {
                        return false;
                    }
                    cellTo = cell.cellLeft;
                    break;
                case ENTITY_DIRECTION.RIGHT:
                    if (cell.cellRight == null || cell.cellRight.componentCell.IsVisible())
                    {
                        return false;
                    }
                    cellTo = cell.cellRight;
                    break;
                case ENTITY_DIRECTION.UP:
                    // player can't go  upper 15% of the grid height
                    if (cell.y >= data.yMax)
                    {
                        return false;
                    }

                    if (cell.cellUp == null || cell.cellUp.componentCell.IsVisible())
                    {
                        return false;
                    }
                    cellTo = cell.cellUp;
                    break;
                case ENTITY_DIRECTION.DOWN:
                    if (cell.cellDown == null || cell.cellDown.componentCell.IsVisible())
                    {
                        return false;
                    }
                    cellTo = cell.cellDown;
                    break;
            }

            this.onMoveEnd = onMoveEnd;

            cellMove.Init(data.speed, cell, cellTo);
            
            cell = cellTo;
            cell.componentEntity.Add(this);
            goTo = true;
            return true;
        }

        void OnMovedToCell()
        {
            onMoveEnd();
            goTo = false;
        }

        public void Init(CellData cell)
        {
            if(cellMove == null)
            {
                cellMove = new CellMove();
                cellMove.onMoveEnd = OnMovedToCell;
                projectile = goProjectile.AddComponent<ComponentAssetProjectile>();
            }

            this.cell = cell;
            cell.componentEntity.Add(this);
            life = data.life;
            transform.position = cell.componentCell.transform.position;
            enabled = false;
        }

        public void Shoot(Action onShootEnd)
        {
            this.onShootEnd = onShootEnd;
            projectile.FireShoot(data.shootSpeed, cell, OnProjectileReachedTarget);
        }

        public override void Hit(byte value)
        {
            Death();
            onDeath();
            enabled = false;
        }

        void OnProjectileReachedTarget()
        {
            onShootEnd();
        }

        public void SetPlayerData(PlayerData data)
        {
            this.data = data;
        }

        public CellData GetUpperCell()
        {
            return cell.cellUp;
        }

        public void State(bool state, Action onDeath=null)
        {
            this.onDeath = onDeath;
            goGraphic.SetActive(state);
            goDeath.SetActive(false);
            goProjectile.SetActive(false);
            enabled = state;
        }
    }
}
