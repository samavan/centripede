﻿using System;
using UnityEngine;

namespace Atari.Centripede
{
    public class ComponentAssetProjectile : MonoBehaviour
    {
        byte speed;
        CellMove cellMove;

        Action onReachedTarget;
        public CellData cell;

        ComponentAsset entityFound;

        private void Start()
        {
            cellMove = new CellMove();
            cellMove.onMoveEnd = OnReachedCell;
            gameObject.SetActive(false);
        }

        private void LateUpdate()
        {
            transform.position = cellMove.GoTo();

            entityFound = ManagerGrid.CheckForEnemyInCell(cell);
            if (entityFound)
            {
                entityFound.Hit(1);
                ReachedEnd();
                return;
            }
        }

        void ReachedEnd()
        {
            onReachedTarget();
            gameObject.SetActive(false);
        }

        public void FireShoot(byte speed, CellData cellFrom, Action onReachedTarget)
        {
            this.speed = speed;
            this.cell = cellFrom;
            this.onReachedTarget = onReachedTarget;
            cellMove.Init(speed, cell.cellUp, cell.cellUp.cellUp);
            gameObject.SetActive(true);
        }

        void OnReachedCell()
        {
            if (cell.cellUp == null)
            {
                ReachedEnd();
                return;
            }

            cell = cell.cellUp;

            /*
            if (cell.componentEntity)
            {
                cell.componentEntity.Hit(1);
                ReachedEnd();
                return;
            }
            */

            if (cell.componentCell.IsVisible())
            {
                cell.componentCell.Hit(1);
                ReachedEnd();
                return;
            }

            if (cell.cellUp == null)
            {
                ReachedEnd();
                return;
            }
            cellMove.Init(speed, cell, cell.cellUp);           
        }

        public void OnDrawGizmosSelected()
        {
            if (cell != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(transform.position, cell.componentCell.transform.position + Vector3.forward * 0.5f);
            }
        }
    }
}
