﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class GridFactory : MonoBehaviour
    {
        public void Save()
        {
            grid.blockIdxToShow = new List<int>();

            for(int i=0; i < cells.Count; i++)
            {
                if (cells[i].componentCell.IsVisible())
                {
                    grid.blockIdxToShow.Add(i);
                }
            }

            string json = JsonUtility.ToJson(grid, true);

#if UNITY_EDITOR
            string dir = Application.dataPath + "/" + 
                ManagerApp.Directory.resourcesRoot + 
                ManagerApp.Resources.level;

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            string[] filesInDir = Directory.GetFiles(dir, "*.txt");

            string file = filesInDir.Length.ToString();

            for(int i=file.Length; i < 3; i++)
            {
                file = "0" + file;
            }

            file += ".txt";

            File.WriteAllText(dir + file, json);

            Debug.Log("<color=green> Grid Saved : </color>" + dir + file + ";");
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

        public GridData Load(string saveName)
        {
            string res = ManagerApp.Resources.level + saveName;
            TextAsset ta = Resources.Load(res) as TextAsset;
            if(ta.text == "")
            {
                Debug.Log("<color=red>Something went wront while loading grid save : </color>" + res + ";");
                return null;
            }

            GridData grid = JsonUtility.FromJson<GridData>(ta.text);

            return grid;
        }
    }
}