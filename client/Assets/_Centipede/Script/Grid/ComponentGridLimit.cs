﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public class ComponentAssetGridLimit : MonoBehaviour
    {
        // this class to avoid player projectile to over the grid area.

        BoxCollider col;

        private void Start()
        {
            col = gameObject.AddComponent<BoxCollider>();
        }
    }
}
