﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    [Serializable]
    public class GridData
    {
        public byte width;
        public byte height;
        public float depth;
        
        public int xMax;
        public int yMax;
        public int cellCount;
        public List<int> blockIdxToShow;
    }
}
