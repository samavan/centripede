﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    [Serializable]
    public class CellData
    {
        public ComponentAssetCell componentCell;
        public List<ComponentAsset> componentEntity = new List<ComponentAsset>();      // current entity on the cell (player, enemy, etc...)

        public byte life;

        public int x;
        public int y;

        public Vector3 position;

        // adjacentCells
        // used for player move, path finding...
        public CellData cellDown;
        public CellData cellLeft;
        public CellData cellRight;
        public CellData cellUp;
    }
}
