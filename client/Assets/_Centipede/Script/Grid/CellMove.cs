﻿using System;
using UnityEngine;

namespace Atari.Centripede
{
    public class CellMove
    {
        float speed;
        public Action onMoveEnd;
        public bool isBusy;

        Vector3 position;

        Vector3 posFrom;
        Vector3 posTo;
        float moveProgress;


        public Vector3 GoTo()
        {
            moveProgress = Mathf.Clamp(moveProgress + Time.deltaTime * speed, 0, 1);
            position = Vector3.Lerp(posFrom, posTo, moveProgress);

            if (moveProgress >= 1)
            {
                End();
            }

            return position;
        }

        public void Init(float speed, CellData cellFrom, CellData cellTo)
        {
            this.speed = speed;
            posFrom = cellFrom.componentCell.transform.position;
            posTo = cellTo.componentCell.transform.position;
            moveProgress = 0;
            isBusy = true;
        }

        void End()
        {
            isBusy = false;
            onMoveEnd();
        }
    }
}
