﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class GridFactory : MonoBehaviour
    {
        const string resAsset = "CEL_00";
        ComponentAssetGridLimit gridLimitTop;

        public GridData grid;

        [HideInInspector]
        public List<CellData> cells = new List<CellData>();
        Dictionary<int, List<CellData>> gridCells;

        [HideInInspector]
        public CellData playerStartCell;
        [HideInInspector]
        public CellData centripedeStartCell;
        [HideInInspector]
        public CellData spiderStartCell;
        

        private void Start()
        {
            gridLimitTop = new GameObject("GridLimit_Top").AddComponent<ComponentAssetGridLimit>();
            gridLimitTop.transform.parent = transform;
        }

        public void ClearData()
        {
            foreach (CellData cell in cells)
            {
                cell.componentEntity = new List<ComponentAsset>();
            }
        }

        public void ShowBlocks(List<CellData> cells, List<int> idxLs)
        {
            foreach(int idx in idxLs)
            {
                cells[idx].componentCell.State(true);
            }
        }

        bool AllowedCell(CellData cell)
        {
            if (cell.y == grid.yMax)
            {
                return false;
            }
            if (cell.x == 0 || cell.x == grid.xMax) // to avoit the centipede to be stucked... but hmmm...
            {
                return false;
            }
            else if (cell.x == playerStartCell.x && cell.y == playerStartCell.y)
            {
                return false;
            }
            else if (cell.x == spiderStartCell.x && cell.y == spiderStartCell.y)
            {
                return false;
            }


            return true;
        }

        void DebugLog(string log)
        {
            // Debug.Log("<color=grey>GridFactory : </color>" + log + ";");
        }
    }
}