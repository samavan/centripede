﻿using UnityEngine;

namespace Atari.Centripede
{
    public class ManagerGrid
    {
        public static ComponentAsset CheckForPlayerInCell(CellData cell)
        {
            foreach (ComponentAsset entity in cell.componentEntity)
            {
                if (entity && entity as ComponentAssetPlayer)
                {
                    return entity;
                }
            }
            return null;
        }
        public static ComponentAsset CheckForEnemyInCell(CellData cell)
        {
            foreach (ComponentAsset entity in cell.componentEntity)
            {
                if (entity && !(entity as ComponentAssetPlayer))
                {
                    return entity;
                }
            }
            return null;
        }

        public static CellData ClosestCellFromTargetCell(CellData[] cells, CellData cellTarget)
        {
            // if by chance cells content the target cell
            foreach(CellData _cell in cells)
            {
                if (_cell == cellTarget)
                {
                    return _cell;
                }
            }

            CellData cell = null;
            float distanceTmp = 0;
            float distanceTest = 999999;

            for (int i = 0; i < 4; i++)
            {
                if (cells[i] == null || cells[i].componentCell.IsVisible())
                {
                    continue;
                }

                distanceTmp = Vector2.Distance(cells[i].position, cellTarget.position);
                if (distanceTmp < distanceTest)
                {
                    distanceTest = distanceTmp;
                    cell = cells[i];
                }
            }
            return cell;
        }

    }
}
