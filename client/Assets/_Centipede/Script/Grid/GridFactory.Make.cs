﻿using System.Collections.Generic;
using UnityEngine;

namespace Atari.Centripede
{
    public partial class GridFactory : MonoBehaviour
    {
        GameObject goCell;
        Texture2D[] lifeTextures;

        public void NewRandomGrid(GridData grid, ushort visibleSlot, byte cellLife)
        {
            this.grid =     grid;
            grid.cellCount = grid.width * grid.height;
            MakeGrid(grid, cellLife);
            AddRandomBlock(visibleSlot);
        }

        public void NewGridFromData(GridData grid, byte cellLife)
        {
            this.grid = grid;
            MakeGrid(grid, cellLife);
            ShowBlocks(cells, grid.blockIdxToShow);
        }

        public void MakeGrid(GridData grid, byte cellLife)
        {
            if(!goCell)
            {
                goCell = Instantiate(Resources.Load(ManagerApp.Resources.cell + resAsset) as GameObject);
                goCell.SetActive(false);
            }

            // texture for life graphic
            lifeTextures = ManagerMushroom.GetLifeTextures(
                cellLife, 
                (Texture2D)goCell.GetComponent<ComponentAssetCell>().goGraphic.GetComponentInChildren<Renderer>(true).material.mainTexture);

            gridCells = new Dictionary<int, List<CellData>>();

            // grid top limit : to stop the projectile;
            gridLimitTop.transform.position = new Vector3(grid.width * 0.5f, grid.height + 5, 0);
            gridLimitTop.transform.localScale = new Vector3(grid.width, 10, 1);


            DebugLog("to generate cells if need...");

            if (grid.cellCount >= cells.Count)
            {
                CellData data;

                for (int i=cells.Count; i < grid.cellCount; i++)
                {
                    data = new CellData();
                    data.componentCell = (GameObject.Instantiate(goCell) as GameObject).GetComponent<ComponentAssetCell>();
                    data.componentCell.gameObject.name = i + "-Cell";
                    data.componentCell.debugIdx.text = i.ToString();
                    data.componentCell.gameObject.SetActive(true);
                    data.componentCell.transform.parent = transform;
                    data.componentCell.transform.Reset();
                    cells.Add(data);
                }
            }

            DebugLog("cells position & data");

            int x =0, y, idx=0;

            while (x < grid.width)
            {
                gridCells.Add(x, new List<CellData>());
                y = 0;
                while (y < grid.height)
                {
                    cells[idx].life = cellLife;
                    cells[idx].x = x;
                    cells[idx].y = y;
                    cells[idx].position = new Vector3(x + 0.5f, y + 0.5f, grid.depth);
                    cells[idx].componentCell.lifeTextures = lifeTextures;
                    cells[idx].componentCell.data = cells[idx];
                    cells[idx].componentCell.life = cellLife;
                    cells[idx].componentCell.Init(new Vector3(cells[idx].position.x, cells[idx].position.y, grid.depth));
                    gridCells[x].Add(cells[idx]);
                    idx++;
                    y++;
                }
                x++;
            }

            DebugLog("entities start cell");

            centripedeStartCell = gridCells[0][grid.height - 1];
            playerStartCell = gridCells[Mathf.CeilToInt(grid.width * 0.5f) - 1][0];
            spiderStartCell = gridCells[Mathf.CeilToInt(grid.width * 0.5f) - 1][Mathf.CeilToInt(grid.height * 0.5f) - 1];

            DebugLog("to assign adjacent cell");

            x = 0;
            grid.xMax = grid.width - 1;
            grid.yMax = grid.height - 1;
            while (x < grid.width)
            {
                y = 0;
                while (y < grid.height)
                {
                    if (x > 0)
                    {
                        gridCells[x][y].cellLeft = gridCells[x-1][y];
                    }
                    if (x < grid.xMax)
                    {
                        gridCells[x][y].cellRight = gridCells[x+1][y];
                    }
                    if (y > 0)
                    {
                        gridCells[x][y].cellDown = gridCells[x][y-1];
                    }
                    if (y < grid.yMax)
                    {
                        gridCells[x][y].cellUp = gridCells[x][y+1];
                    }
                    y++;
                }
                x++;
            }

            // hide every cells
            foreach (CellData cell in cells)
            {
                cell.componentCell.State(false);
            }
        }

        public CellData[] AddRandomBlock(ushort count)
        {
            List<int> randomIdx = new List<int>();
            CellData[] result = new CellData[count];
            int idx = 0;
            int value;
            int yMax = grid.height - 1;

            while (idx < count)
            {
                value = Mathf.Clamp(Mathf.CeilToInt(Random.Range(0, grid.cellCount + 1)), 0, grid.cellCount - 1);
                if (!randomIdx.Contains(value)
                    && AllowedCell(cells[value]))
                {
                    randomIdx.Add(value);
                    result[idx] = cells[value];
                    cells[value].componentCell.State(true);
                    idx++;
                }
            }
            return result;
        }
    }
}