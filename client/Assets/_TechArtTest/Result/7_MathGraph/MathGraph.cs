﻿using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.TechArtTest
{
    public class MathGraph : MonoBehaviour
    {
        [Header("## User Settings ##")]
        public int quality = 50;
        public Vector2 range = new Vector3(0, 10);
        public float heightCoef = 1;

        // graphic assets
        List<Transform> transforms = new List<Transform>();

        private void Start()
        {
            MakeGraphicAssets();
        }

        private void Update()
        {
            UpdatePosition();
        }

        void MakeGraphicAssets()
        {
            float coefX = (range.y - range.x) / (int)quality;
            for (int i = 0; i < quality; i++)
            {
                transforms.Add(GameObject.CreatePrimitive(PrimitiveType.Cube).transform);
                transforms[i].gameObject.name = "CurveKnot_" + i;
                transforms[i].parent = transform;
                transforms[i].position = new Vector3(coefX * i, 0, 0);
                transforms[i].localScale = Vector3.one * 0.1f;
            }
        }

        // to avoid per frame declaration.
        Vector3 position;
        float height;

        private void UpdatePosition()
        {
            // to avoid zero value from user input.
            heightCoef = Mathf.Clamp(heightCoef, 0.001f, Mathf.Infinity);
            height = heightCoef * 2;

            for (int i = 0; i < quality; i++)
            {
                position = transforms[i].position;
                //position.y = Mathf.Cos(Mathf.PI * (position.x ));
                position.y = Mathf.Abs((position.x % height) - heightCoef);
                transforms[i].position = position;
            }
        }

    }
}
