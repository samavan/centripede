﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.TechArtTest
{
    public class JumpingSphereFactory : MonoBehaviour
    {
        [Header("## User Settings ##")]
        public int sphereCount = 9;
        public Vector2 speedRange = new Vector2(0.95f, 1.05f);
        public Texture texture;
        public Color optionalColor;

        AnimationClip jumpingClip;
        GameObject goGround;
        List<GameObject> goSpheres;

        // Not sure what is the purpose of this exercise but as I could use ease curve to make this animation...
        // could also been done with some vertex shader... well it's a bit confusing.
        // Then for a simple Jumping sphere jumping without any interaction I will just use a legacy anim...
        // As written in the documentation, Legacy is still a super cheap memory use and simple to get it works.
        // Also Legacy simple clip Versus custom script update, I guess legacy is cheaper as well.

        void Start()
        {
            // catch the anim from resource
            jumpingClip = Resources.Load("LegacyCLip/JumpingSphere=idle_00") as AnimationClip;
            
            // prepare the scene.
            goGround = MakeGround();
            MakeSpheres(sphereCount);
        }

        
        void MakeSpheres(int count)
        {
            goSpheres = new List<GameObject>();
            int index = 0;
            int cellSize = 2;
            int gridWidth = (int)Mathf.Ceil(Mathf.Sqrt(count));
            float posStart = -(gridWidth + cellSize * 0.5f) * 0.5f;

            Shader shader = Shader.Find("Unlit/TestUnlitShader");

            for (int x=0; x< gridWidth; x++)
            {
                for (int z = 0; z < gridWidth; z++)
                {
                    // make sphere & assign a random speed.
                    // could also an async Coroutine, but well.
                    goSpheres.Add(MakeSphere(index, jumpingClip, Random.Range(speedRange.x, speedRange.y)));

                    // position
                    goSpheres[index].transform.position = new Vector3(
                        posStart + x* cellSize, 
                        0.5f, 
                        posStart + z * cellSize);

                    // apply shader on even index
                    if (index%2==0)
                    {
                        Material mat = goSpheres[index].GetComponentInChildren<MeshRenderer>(true).material;
                        mat.shader = shader;
                        mat.color = optionalColor;
                        mat.mainTexture = texture;
                        mat.mainTextureScale = new Vector2(4, 2);
                    }
                    index++;
                }
            }
        }

        GameObject MakeSphere(int idx, AnimationClip clip, float speed)
        {
            // make new sphere object
            GameObject go = new GameObject("JumpingSphere_" + idx);
            go.transform.parent = transform;
            GameObject goSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            goSphere.transform.parent = go.transform;

            // setup legacy animation
            Animation anim = go.AddComponent<Animation>();
            anim.playAutomatically = true;
            anim.AddClip(clip, clip.name);
            anim.Play(clip.name);

            // the trick to have asynchrone anim
            anim[clip.name].time = Random.Range(0, clip.length);

            // more trick
            /*
            foreach (AnimationState state in anim)
            {
                state.speed = speed;
            }
            */

            return go;
        }

        GameObject MakeGround()
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Plane);
            Material mat = new Material(Shader.Find("Unlit/Ground"));
            mat.mainTexture = texture;
            mat.mainTextureScale = new Vector2(5, 5);
            go.GetComponent<MeshRenderer>().material = mat;
            return go;
        }
    }
}
