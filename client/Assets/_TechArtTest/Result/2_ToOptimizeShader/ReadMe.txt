Those weird parts are removed :

//	float4 temp = 1024;  // finally no use
//	temp /= pow(2, 10); // <<<< ????   pow(2, 10) = 1024 >>> 1024 / 1024 = 1 >> always 1? what's the point.

ambient = ShadeSH9(float4(worldNormal.xyz, 1)).rgbb;			//  *temp.rrrr; >> didn't see any diff without.
nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));		// temp.yyyy;	>> didn't see any diff without.

But well those errors could be found in C# as well.
The shader is already a Fragment shader then I am ok with it I guess.