﻿using UnityEngine;

namespace TrueAxion.TechArtTest.ProceduralWall
{
    public partial class ToolProceduralWall : MonoBehaviour
    {      
        Transform NewRoot(string baseName)
        {
            Transform tr = new GameObject("_" + baseName + "_").transform;
            tr.parent = transform;
            tr.localPosition = Vector3.zero;
            tr.localScale = Vector3.one;
            return tr;
        }

        LineData NewLine(AssetTemplateData [] assetsData, 
            ComponentWallNode nodeStart,
            ComponentWallNode nodeEnd)
        {
            LineData line = new LineData();
            line.assets = assetsData;
            line.start = nodeStart;
            line.end = nodeEnd;
            lines.Add(line);
            return line;
        }

        ComponentWallNode AddNode(Vector3 position)
        {
            ComponentWallNode node = NewNode(nodes.Count.ToString());
            node.transform.position = position;
            nodes.Add(node);

            if(currentNode)
            {
                node.prevNodes.Add(currentNode);
                currentNode.nextNodes.Add(node);
            }

            return node;
        }

        ComponentWallNode NewNode(string baseName)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.name = "Node_" + baseName;
            Transform tr = go.transform;
            tr.parent = trNodeRoot;
            tr.localScale = Vector3.one;
            return go.AddComponent<ComponentWallNode>();
        }

        ComponentGround NewGround()
        {
            ComponentGround ground = GameObject.CreatePrimitive(PrimitiveType.Cube).AddComponent<ComponentGround>();
            ground.gameObject.name = "_ground_";
            Transform tr = ground.transform;
            tr.parent = transform;
            tr.localScale = new Vector3(50, 1, 50);
            tr.position = Vector3.up * -0.5f;
            //Destroy(ground.GetComponent<MeshRenderer>());
            return ground;
        }

        public GameObject NewAsset(GameObject go)
        {
            GameObject goCopy = Instantiate(go) as GameObject;
            goCopy.name = go.name;
            goCopy.transform.parent = trAssetRoot;
            return goCopy;
        }
    }
}
