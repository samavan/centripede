﻿using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.TechArtTest.ProceduralWall
{
    public partial class ToolProceduralWall : MonoBehaviour
    {
        // for this esercice let have every wall with the same lenght
        // we can always make it more procedural, more customized, etc...
        // but for the test let keep it simple.

        [Header("--- User Define ---")]
        public GameObject [] wallTemplates;

        // data related to the asset (size, vertex, etc...)
        AssetTemplateData [] assetsData;
        
        // nodes for user interaction
        List<ComponentWallNode> nodes = new List<ComponentWallNode>();
        ComponentWallNode currentNode;

        // lines mades from nodes
        List<LineData> lines = new List<LineData>();
        List<LineData> currentLines = new List<LineData>(); // moving one node cann affect several lines.
        
        // tool asset hierarchy
        ComponentGround ground;
        Transform trAssetRoot;
        Transform trNodeRoot;

        private void Start()
        {
            assetsData = new AssetTemplateData[wallTemplates.Length];
            for(int i=0; i < assetsData.Length; i ++)
            {
                assetsData[i] = GetAssetTemplateData(wallTemplates[i]);
            }

            trAssetRoot = NewRoot("Asset");
            trNodeRoot = NewRoot("WallNode");

            ground = NewGround();
        }

        private void Update()
        {
            Controller();
        }

        void SetAsActiveNode(ComponentWallNode node)
        {
            if (currentNode)
            {
                currentNode.SetActive(false);
            }

            node.SetActive(true);
            currentNode = node;
        }

        AssetTemplateData GetAssetTemplateData(GameObject go)
        {
            AssetTemplateData data = new AssetTemplateData();
            data.go = go;
            // data.size = go.GetComponentInChildren<MeshFilter>(true).sharedMesh.bounds;
            data.size = go.transform.localScale;
            return data;
        }

        List<LineData> GetLinesFromNode(ComponentWallNode node)
        {
            List<LineData> lines = new List<LineData>();
            foreach(LineData line in this.lines)
            {
                if (line.start == node || line.end == node)
                {
                    lines.Add(line);
                }
            }

            return lines;
        }
    }
}
