﻿using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.TechArtTest.ProceduralWall
{
    public partial class ComponentWallNode : MonoBehaviour
    {
        [HideInInspector]
        public List<ComponentWallNode> prevNodes = new List<ComponentWallNode>();
        [HideInInspector]
        public List<ComponentWallNode> nextNodes = new List<ComponentWallNode>();

        Material mat;
        bool isActive;

        public void SetActive(bool state)
        {
            isActive = state;
            if (!mat)
            {
                mat = gameObject.GetComponent<Renderer>().material;
            }

            Color color = state ? Color.red : Color.blue;
            color.a = 0.5f;
            mat.color = color;
        }

        void OnDrawGizmosSelected()
        {
            if (isActive)
            {
                Gizmos.color = Color.red;

                foreach (ComponentWallNode node in prevNodes)
                {
                    Gizmos.DrawLine(transform.position, node.transform.position);
                }

                foreach (ComponentWallNode node in nextNodes)
                {
                    Gizmos.DrawLine(transform.position, node.transform.position);
                }
            }
            else
            {
                Gizmos.color = new Color(0,0,1,0.2f);

                foreach (ComponentWallNode node in prevNodes)
                {
                    Gizmos.DrawLine(transform.position, node.transform.position);
                }

                foreach (ComponentWallNode node in nextNodes)
                {
                    Gizmos.DrawLine(transform.position, node.transform.position);
                }
            }
        }
    }
}
