﻿using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.TechArtTest.ProceduralWall
{
    public partial class ToolProceduralWall : MonoBehaviour
    {
        Camera _camera;
        RaycastHit[] hits;
        RaycastHit closestHit;
        float closetDistance;
        float distanceTest;
        Vector3 camPos;

        bool isDragging;

        void Controller()
        {
            if (isDragging)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    isDragging = false;
                    return;
                }

                UserDrag(currentNode.transform);
                UpdateLines(currentLines);
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                isDragging = UserClick();
            }
        }


        bool UserClick()
        {
            _camera = Camera.main;

            hits = Physics.RaycastAll(_camera.ScreenPointToRay(Input.mousePosition));

            if (hits.Length > 0)
            {
                // Debug.Log("IN");
                camPos = _camera.transform.position;

                // 1. closest UserClick hit from camera
                closetDistance = 999999;
                closestHit = new RaycastHit();

                foreach (RaycastHit _hit in hits)
                {
                    if (!_hit.transform.gameObject.GetComponentInParent<ComponentGround>() &&
                        !_hit.transform.gameObject.GetComponentInParent<ComponentWallNode>())
                    {
                        continue;
                    }

                    distanceTest = Vector3.Distance(_hit.point, camPos);
                    if (distanceTest < closetDistance)
                    {
                        closetDistance = distanceTest;
                        closestHit = _hit;
                    }
                }

                if (!closestHit.transform)
                {
                    return false;
                }

                GameObject go = closestHit.transform.gameObject;

                ComponentWallNode node = null;

                if (go.GetComponentInParent<ComponentGround>())
                {
                    node = AddNode(closestHit.point);
                }
                else if (go.GetComponentInParent<ComponentWallNode>())
                {
                    node = go.GetComponentInParent<ComponentWallNode>();
                }

                if (node)
                {
                    SetAsActiveNode(node);
                    currentLines = GetLinesFromNode(currentNode);
                    if (currentLines.Count == 0
                        && currentNode.prevNodes.Count > 0)
                    {
                        currentLines.Add(NewLine(assetsData, currentNode.prevNodes[0], node));
                    }
                    return true;
                }
            }
            return false;
        }

        void UserDrag(Transform tr)
        {
            _camera = Camera.main;

            hits = Physics.RaycastAll(_camera.ScreenPointToRay(Input.mousePosition));

            if (hits.Length > 0)
            {
                // Debug.Log("IN");
                camPos = _camera.transform.position;

                // 1. closest UserClick hit from camera
                closetDistance = 999999;
                closestHit = new RaycastHit();

                foreach (RaycastHit _hit in hits)
                {
                    if (!_hit.transform.gameObject.GetComponentInParent<ComponentGround>())
                    {
                        continue;
                    }

                    distanceTest = Vector3.Distance(_hit.point, camPos);
                    if (distanceTest < closetDistance)
                    {
                        closetDistance = distanceTest;
                        closestHit = _hit;
                    }
                }

                if (!closestHit.transform)
                {
                    return;
                }
                tr.position = closestHit.point;
            }
        }
    }
}
