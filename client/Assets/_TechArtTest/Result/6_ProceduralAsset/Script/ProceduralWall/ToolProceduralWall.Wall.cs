﻿using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.TechArtTest.ProceduralWall
{
    public partial class ToolProceduralWall : MonoBehaviour
    {
        class AssetTemplateData
        {
            public GameObject go;
            public Vector3 size; // bouding box
//            public Vector3 baseScale; // could happen asset from Maya or else inherite of a 0.1f scale.
        }

        class LineData
        {
            public AssetTemplateData [] assets;
            public ComponentWallNode start;
            public ComponentWallNode end;

            public int assetCount;
            public List<Transform> trLs; // transforms visbile
            public List<List<Transform>> trLibrary; // every transfrom available
        }

        // to avoid redeclaration/allocaton every frame...
        int i;
        int randAsset;
        float distance;
        float assetCount;
        float assetCountDiff;
        Vector3 posStart;
        Vector3 posEnd;
        Vector3 position;
        Vector3 positionCoef;
        Vector3 eulerAngle;
        Vector3 scale = new Vector3(1,1,1);
        Transform trAsset;

        void UpdateLines(List<LineData> lines)
        {
            foreach(LineData line in lines)
            {
                UpdateLine(line);
            }
        }

        void UpdateLine(LineData line)
        {
            posStart = line.start.transform.position;
            posEnd = line.end.transform.position;

            // nodes distance
            distance = Vector3.Distance(posStart, posEnd);

            // how many asset lenght in the distance
            assetCount = distance / (line.assets[0].size.x);

            // count cannot be perfect due to the distance
            // to take the float from the assetCount and to adjust the asset scale
            assetCountDiff = assetCount - Mathf.Floor(assetCount);
            assetCount = Mathf.Floor(assetCount);
            scale = line.assets[0].size; // line.assets[0].baseScale;
            scale.x += (assetCountDiff / (assetCount)); // * line.assets[0].baseScale.x;

            Debug.Log(assetCount);

            // some other preparation
            eulerAngle.y = Angle(posStart, posEnd);

            if (line.trLibrary == null)
            {
                line.trLibrary = new List<List<Transform>>();
                foreach(AssetTemplateData asset in line.assets)
                {
                    line.trLibrary.Add(new List<Transform>());
                }

                line.trLs = new List<Transform>();
            }

            // Remove or add assets to the Transform list

            if (assetCount < line.trLs.Count)
            {
                Debug.Log("remove asset");
                for (i = line.trLs.Count - 1; i >= assetCount; i--)
                {
                    line.trLs[i].gameObject.SetActive(false);
                    line.trLs.Remove(line.trLs[i]);
                }
            }
            else if (assetCount > line.trLs.Count)
            {
                Debug.Log("add more asset");
                for (i = line.trLs.Count - 1; i < assetCount; i++)
                {
                    trAsset = null;
                    randAsset = (int)Mathf.Clamp(Random.Range(0, line.assets.Length), 0, line.assets.Length - 1);

                    foreach (Transform tr in line.trLibrary[randAsset])
                    {
                        if (!line.trLs.Contains(tr))
                        {
                            Debug.Log("new asset");
                            trAsset = tr;
                            break;
                        }
                    }

                    if (!trAsset)
                    {
                        trAsset = NewAsset(line.assets[randAsset].go).transform;
                        line.trLibrary[randAsset].Add(trAsset);
                    }
                    line.trLs.Add(trAsset);
                }
            }

            // assets final setup (position, euler angle, scale).

            positionCoef = (posEnd - posStart) / Mathf.Clamp((assetCount), 1, Mathf.Infinity);

            for (i=0; i < line.trLs.Count; i++)
            {
                line.trLs[i].gameObject.SetActive(true);
                line.trLs[i].position = posStart + positionCoef * i;
                line.trLs[i].eulerAngles = eulerAngle;
                line.trLs[i].localScale = scale;
            }

            line.assetCount = (int)assetCount;


            // if Line is connected to the previous or next node

            // 1. to deform first asset

            // 2. to disform last asset
        }

        float Angle(Vector3 a, Vector3 b)
        {
            return Mathf.Atan2(a.z - b.z, b.x - a.x) * 180 / Mathf.PI;
        }
    }
}
