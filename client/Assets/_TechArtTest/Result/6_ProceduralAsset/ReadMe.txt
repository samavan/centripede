-------------------------------------------------------------------------------------------

1. Press play
2. Make a first Node by clicking on the ground.
3. The node is Red.
4. To build a first line of wall : click & drag to the same time on the ground.
5. Release the click to valid the wall.
6. A second Node has been created to the end on that new line.
7. the first node become Blue, the new node is Red.

-------------------------------------------------------------------------------------------

8. Repeat the process from. to make another line of wall

-------------------------------------------------------------------------------------------

9. Note that the last Node created always become red, and previous are blue.

-------------------------------------------------------------------------------------------

10. To edit a line of wall : Click on a node (become red) and drag and drop it.

-------------------------------------------------------------------------------------------

11. To start a line of wall from an existing node : selected it (become red) process from 4.
12. Note : new wall is always created from the Red node.

-------------------------------------------------------------------------------------------
