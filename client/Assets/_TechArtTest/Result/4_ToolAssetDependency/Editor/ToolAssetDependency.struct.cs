﻿using System.Collections.Generic;
using UnityEditor;

namespace TrueAxion.TechArtTest
{
    public partial class ToolAssetDependency : EditorWindow
    {
        public struct ToolAssetDependencyData
        {
            public AssetInfo asset;

            // optimized search
            public bool optimizedSearch;
            public bool filterMaterial;
            public bool filterPrefab;
            public bool filterScene;
            //public bool filterOther;

            // dependency result
            public List<string> filesToRead;

            // dependency result
            public FileDependencyResult unity;
            public FileDependencyResult script;
            public Dictionary<string, List<FileInfo>> result;

            // misc
            public bool processSearch;
        }
        public struct AssetInfo
        {
            public string name;
            public string fileType;
            public string absolutePath;
            public string projectPath;
            public string guid;
        }

        public struct FileDependencyResult
        {
            public List<string> files;
            public List<string> matches;
            public int idx; // only used while processing
        }

        public struct FileInfo
        {
            public string name;
            public string path;
            public string projectPath;
        }
    }
}
