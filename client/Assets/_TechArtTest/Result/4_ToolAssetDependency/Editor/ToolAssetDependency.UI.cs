﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace TrueAxion.TechArtTest
{
    public partial class ToolAssetDependency : EditorWindow
    {
        static string toolName = "Asset Dependecy";

        // super short variable name because it's gonna be everywhere.
        // "d" as data.
        static ToolAssetDependencyData d;

        [MenuItem("TechArt-Tool/4 _ Asset Dependency")]
        static void Dialog()
        {
            d = new ToolAssetDependencyData();
            ShowWindow();
        }

        static void ShowWindow()
        {
            EditorWindow window = GetWindow(typeof(ToolAssetDependency), true, toolName, true);
            window.name = toolName;
            window.Show();
        }

        void OnGUI()
        {
            Ray ray = SceneView.lastActiveSceneView.camera.ScreenPointToRay(Event.current.mousePosition);

            WindowDraw();

            if (d.asset.guid == null || d.asset.guid == "") { return; }

            if (d.processSearch)
            {
                DataProcessing();
            }
        }

        void OnInspectorUpdate()
        {
            Repaint();
        }

        static void Dialog(string msg)
        {
            EditorUtility.DisplayDialog(toolName, msg, "OK");
        }


        // ================================================== //
        // ================ WINDOW DRAW ===================== //
        // ================================================== //

        void WindowDraw()
        {
            if (GUILayout.Button("1. Select asset in Project tab.\n2. Press to refresh window."))
            {
                d = new ToolAssetDependencyData();
                if (!Init())
                {
                    return;
                }
                d.processSearch = true;
            }

            if (d.asset.guid == null || d.asset.guid == "") { return; }

            GUIAssetInfo();
            OptimizedSearch();

            if (d.result == null) { return; }

            Result();
        }

        void GUIAssetInfo()
        {
            GUISeparator();
            GUILayout.Label("Asset Info", EditorStyles.boldLabel);
            GUIInfoLine("Name", d.asset.name);
            GUIInfoLine("GUID", d.asset.guid);
            GUISearchLine("Unity dependency", d.unity);
            GUISearchLine("Script dependency", d.script);

            if (GUILayout.Button("Select in Project"))
            {
                SelectInProject(d.asset.projectPath);
            }
        }

        void GUISearchLine(string label,  FileDependencyResult data)
        {
            if (data.files != null && data.idx >= data.files.Count)
            {
                GUIInfoLine(label, data.matches.Count.ToString());
            }
            else
            {
                GUIInfoLine(label, data.matches.Count +
                    " (still on progress " + data.idx + "/" + data.files.Count + ")");
            }
        }

        void GUIInfoLine(string label, string content)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(label + " :", EditorStyles.boldLabel);
            GUILayout.Label(content);
            GUILayout.EndHorizontal();
        }

        void OptimizedSearch()
        {
            GUISeparator();
            d.optimizedSearch = EditorGUILayout.BeginToggleGroup("Optimize Search", d.optimizedSearch);

            if (d.optimizedSearch)
            {
                GUILayout.Label("(Unselected are removed from search)", EditorStyles.miniLabel);
                d.filterMaterial = EditorGUILayout.Toggle("Material", d.filterMaterial);
                d.filterPrefab = EditorGUILayout.Toggle("Prefab", d.filterPrefab);
                d.filterScene = EditorGUILayout.Toggle("Scene", d.filterScene);
                // d.filterOther = EditorGUILayout.Toggle("Other (unsupported types)", d.filterOther);
            }
            else
            {
                GUILayout.Label("(Target specific files & reduce process time)", EditorStyles.miniLabel);
            }
            EditorGUILayout.EndToggleGroup();
        }

        void Result()
        {
            GUISeparator();
            GUILayout.Label("Result", EditorStyles.boldLabel);
            GUILayout.Label("(press item to hightlight in project)", EditorStyles.miniLabel);
            GUILayout.Label("");
            resultScrollPos = GUILayout.BeginScrollView(resultScrollPos);

            GUILayout.BeginHorizontal();
            
            foreach (KeyValuePair<string, List<FileInfo>> item in d.result)
            {
                GUIResult(item.Key, item.Value);
            }
            
            GUILayout.EndHorizontal();
            GUI.EndScrollView();
        }

        Vector2 resultScrollPos;
        void GUIResult(string label, List<FileInfo> infos)
        {
            GUILayout.BeginVertical();
            GUILayout.Label(label, EditorStyles.boldLabel);
            GUILayout.BeginVertical();
            foreach (FileInfo info in infos)
            {
                if (GUILayout.Button(info.name))
                {
                    SelectInProject(info.projectPath);
                }
            }
            GUILayout.EndVertical();
            GUILayout.EndVertical();
        }      

        void GUISeparator()
        {
            GUILayout.Label("---------------------------");
        }
    }
}
