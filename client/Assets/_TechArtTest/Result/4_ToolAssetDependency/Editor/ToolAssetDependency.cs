﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace TrueAxion.TechArtTest
{
    public partial class ToolAssetDependency : EditorWindow
    {    
        static bool Init()
        {
            // check on user selecton
            var asset = Selection.activeObject;
            if (!asset)
            {
                Dialog("No asset selected. \n>> Select an asset fron the Project tab.");
                return false;
            }

            d.asset.name = asset.name;

            // to find original asset path
            d.asset.projectPath = AssetDatabase.GetAssetPath(asset);
            d.asset.absolutePath = Application.dataPath + "/" + d.asset.projectPath;
            d.asset.absolutePath = d.asset.absolutePath.Replace("/Assets/Assets/", "/Assets/");

            // if it's a folder forget about it
            if (Directory.Exists(d.asset.absolutePath))
            {
                Dialog("Selected asset is a directory.\nIt doesn't depend of anything.");
                return false;
            }

            // asset meta file
            string metaFilePath = d.asset.absolutePath + ".meta";

            if (!File.Exists(metaFilePath))
            {
                Dialog("Meta file doesn't exist :\n" + metaFilePath +
                    "\n See if the meta file are activated from the preferences.");
                return false;
            }

            // Asset guid
            string[] metaContent = File.ReadAllLines(metaFilePath);
            d.asset.guid = "";
            foreach (string str in metaContent)
            {
                if (str.Contains("guid:"))
                {
                    d.asset.guid = str.Replace("guid: ", "");
                    break;
                }
            }

            if (d.asset.guid == "")
            {
                Dialog("meta file found but couldn't fine the guid :\n" + metaFilePath);
                return false;
            }

            // file type 
            d.asset.fileType = Path.GetExtension(d.asset.absolutePath).Replace(".", "");

            // files in projects
            d.filesToRead = new List<string>();
            DeepSearch(Application.dataPath);

            // to remove files not depending of other files
            d.filesToRead = CleanFilesList(d.filesToRead);

            // files filter
            
            d.unity.files = d.filesToRead.Where(_ => !_.Contains(".cs")).ToList();
            d.unity.matches = new List<string>();

            d.script.files = d.filesToRead.Where(_ => _.Contains(".cs")).ToList();
            d.script.matches = new List<string>();

            if (d.optimizedSearch)
            {
                if (!d.filterMaterial)
                {
                    d.unity.files = d.unity.files.Where(_ => !_.Contains(".mat")).ToList();
                }

                if (!d.filterPrefab)
                {
                    d.unity.files = d.unity.files.Where(_ => !_.Contains(".prefab")).ToList();
                }

                if (!d.filterScene)
                {
                    d.unity.files = d.unity.files.Where(_ => !_.Contains(".unity")).ToList();
                }
            }

            // Debug.Log("unity files :\n" + string.Join("\n", d.unity.files));
            // Debug.Log("scripts files :\n" + string.Join("\n", d.script.files));

            return true;
        }

        static void DeepSearch(string dir)
        {
            ProcessDirectory(dir);
        }

        static void ProcessDirectory(string targetDirectory)
        {
            d.filesToRead.AddRange(Directory.GetFiles(targetDirectory));

            string[] subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (string subdirectory in subdirectoryEntries)
            {
                ProcessDirectory(subdirectory);
            }
        }

        static List<string> CleanFilesList(List<string> files)
        {
            // those file can't depend from another file

            // unity files
            var list = files.Where(_ => !_.Contains(".anim"));
            list = list.Where(_ => !_.Contains(".meta"));
            list = list.Where(_ => !_.Contains(".shader"));

            // images
            list = list.Where(_ => !_.Contains(".bmp"));
            list = list.Where(_ => !_.Contains(".jpg"));
            list = list.Where(_ => !_.Contains(".png"));
            list = list.Where(_ => !_.Contains(".psd"));
            list = list.Where(_ => !_.Contains(".tga"));

            // font
            list = list.Where(_ => !_.Contains(".ttf"));

            // audio
            list = list.Where(_ => !_.Contains(".mp3"));
            list = list.Where(_ => !_.Contains(".wav"));

            return list.ToList();
        }

        static bool ParseInFile(string guid, string filePath)
        {
            string[] tmp = File.ReadAllLines(filePath);
            foreach (string line in tmp)
            {
                if (line.Contains(guid))
                {
                    return true;
                }
            }
            return false;
        }

        static void DataProcessing()
        {
            // should be always updated frame per frame due to the large amount of data to process.
            // this way can be stoped any time.

            // unity dependency process
            if (d.unity.idx < d.unity.files.Count)
            {
                if (ParseInFile(d.asset.guid, d.unity.files[d.unity.idx]))
                {
                    d.unity.matches.Add(d.unity.files[d.unity.idx]);
                }
                d.unity.idx++;
            }

            string resourceForm = "" + d.asset.name + "\"";

            // script dependency process
            if (d.script.idx < d.script.files.Count)
            {
                // the file is a script
                // that one is a bit tricky, would require more search and test.
                // easier case : file name and class are the same (monobehavior...)
                if(d.asset.fileType == "cs")
                {
                    if (ParseInFile(d.asset.name, d.script.files[d.script.idx]))
                    {
                        d.script.matches.Add(d.script.files[d.script.idx]);
                    }
                }
                // if not a script : ressources call as "xxxx"
                else
                {
                    if (ParseInFile(resourceForm, d.script.files[d.script.idx]))
                    {
                        d.script.matches.Add(d.script.files[d.script.idx]);
                    }
                }
                d.script.idx++;
            }

            // process complete
            if (d.unity.idx >= d.unity.files.Count &&
                d.script.idx >= d.script.files.Count)
            {
                d.processSearch = false;
                d.result = ProcessResult(d.unity, d.script);
            }
        }

        static Dictionary<string, List<FileInfo>> ProcessResult(
            FileDependencyResult unity,
            FileDependencyResult script)
        {
            Dictionary<string, List<FileInfo>> result = new Dictionary<string, List<FileInfo>>();
            string fileType;

            // UNITY files

            foreach (string file in unity.matches)
            {
                fileType = Path.GetExtension(file).Replace(".", "");
                if (!result.ContainsKey(fileType))
                {
                    result.Add(fileType, new List<FileInfo>());
                }
                result[fileType].Add(NewFileInfo(file));
            }
            
            // SCRIPT files
            result.Add("c#", new List<FileInfo>());
            foreach (string file in script.matches)
            {
                result["c#"].Add(NewFileInfo(file));
            }
            return result;
        }

        static FileInfo NewFileInfo(string file)
        {
            string[] tmp = file.Split('\\');
            FileInfo info = new FileInfo();
            info.name = tmp[tmp.Length-1].Split('.')[0];
            info.path = file.Replace('\\', '/');
            info.projectPath = "Assets" + file.Replace(Application.dataPath, "");
            return info;
        }

        static void SelectInProject(string assetPath)
        {
            var obj = AssetDatabase.LoadMainAssetAtPath(assetPath);
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
    }
}
